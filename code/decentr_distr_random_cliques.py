import numpy as np
import matplotlib.pyplot as plt

import functions
import constants


def run_random_assignment(N, CLASSES, rng, configurations=range(1, 60),
                          shard_size=None, samples_per_class=None,
                          n_shards_per_node=None, test_data=None,
                          return_std=False):
    if test_data is not None:
        all_distributions = test_data
    elif shard_size is not None:
        all_distributions = functions.data_partition(N, CLASSES, rng,
                                                     shard_size,
                                                     samples_per_class,
                                                     n_shards_per_node)
    else:
        all_distributions = rng.uniform(100, 10001, (N, CLASSES))
    distribution = all_distributions.sum(axis=0) / all_distributions.sum()
    average_skew = []
    max_skew, min_skew, std = [], [], []
    for i in range(len(configurations)):
        cliques = (N + configurations[i] - 1) // configurations[i]
        cliques_skew = np.zeros(cliques)
        for j in range(cliques):
            clique_sum = all_distributions[
                         j * configurations[i]:(j + 1) * configurations[
                             i]].sum(axis=0)
            clique_distribution = clique_sum / clique_sum.sum()
            skew = np.abs(clique_distribution - distribution).sum()
            cliques_skew[j] = skew
        average_skew.append(cliques_skew.mean())
        max_skew.append(np.amax(cliques_skew))
        min_skew.append(np.amin(cliques_skew))
        std.append(cliques_skew.std())
    res = [configurations, average_skew, max_skew, min_skew,
           clique_distribution]
    if return_std:
        res.append(std)
    return res


if __name__ == "__main__":
    COLORMAP = {(100, 10): "blue", (600, 50): "orange", (6000, 10): "red",
                (6000, 50): "green"}
    rng = np.random.default_rng(1)

    fig, axs = plt.subplots(figsize=(6, 6))
    axs.set_xlabel("Number of iterations")
    axs.grid(True)
    # for N in [600, 6000]:
    #     for CLASSES in [10, 50]:
    N = constants.ExperimentSetting.N_NODES
    CLASSES = constants.ExperimentSetting.N_CLASSES
    tests = constants.ExperimentSetting.TESTS
    tests_average_skew, tests_max_skew, tests_min_skew, tests_std = [], [], [], []
    for i in range(tests):
        configurations, average_skew, max_skew, min_skew, clique_distribution, std \
            = run_random_assignment(N, CLASSES, rng, range(1, 71),
                                    shard_size=constants.ExperimentSetting.SHARD_SIZE,
                                    samples_per_class=constants.ExperimentSetting.SAMPLES_PER_CLASS,
                                    return_std=True)
        tests_average_skew.append(average_skew)
        tests_max_skew.append(max_skew)
        tests_min_skew.append(min_skew)
        tests_std.append(std)

    tests_average_skew = np.array(tests_average_skew)
    tests_max_skew = np.array(tests_max_skew)
    tests_min_skew = np.array(tests_min_skew)
    tests_std = np.array(tests_std)
    mean_std = tests_std.mean(axis=0)
    average_to_plot = tests_average_skew.mean(axis=0)
    print(configurations)
    print(np.argsort(np.amax(tests_max_skew, axis=0)))
    axs.plot(configurations, average_to_plot,
             c=COLORMAP[(N, CLASSES)],
             label="N = " + str(N) + ", " + str(CLASSES) + " classes"
             )
    # axs.fill_between(configurations, average_to_plot - mean_std,
    #          average_to_plot + mean_std,
    #          color="#888888", alpha=0.4)
    # print(N, CLASSES, min_skew[-1])
    # print(average_skew[-1])
    # print(max_skew[-1])
    # axs.set_ylim(0, 0.5)
    axs.set_ylabel("Average skew")
    axs.set_xlabel("Max number of nodes in a clique")
    axs.plot(configurations, tests_min_skew.min(axis=0), alpha=0.4,
             c=COLORMAP[(N, CLASSES)])
    axs.plot(configurations, tests_max_skew.max(axis=0), alpha=0.4,
             c=COLORMAP[(N, CLASSES)])
    name = f"Skew_size_of_clique_tests_{tests}_without_label"
    # axs.legend(loc='best')
    plt.savefig("../pictures/other/" + name + ".pdf", bbox_inches='tight')
    plt.show()
