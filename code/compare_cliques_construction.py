import numpy as np
import matplotlib.pyplot as plt
import pickle
import os.path
import copy

import decentralized_greedy_resolving_conflicts as MV_greedy
import greedy_with_pre_comp_of_D_changed as greedy_funcs
import decentr_distr_random_cliques as random_alg
import decentralized_greedy_bipartite_graph as bipartite_funcs
import constants


def plot_decentralized_evolution(N_NODES, rng, decentralized_clique,
                                 decentralized_algorithm, axs=None,
                                 directed="", random_sample=10, N_CLASSES=10,
                                 iterations=10, max_n_nodes=None, color="blue",
                                 plot_std=False, plot_max_avg=False,
                                 plot_min_avg=False, legend_label="",
                                 tests=10, shard_size=None,
                                 samples_per_class=None,
                                 n_shards_per_node=None):
    graph_config = {"n": N_NODES, "topology": "RANDOM" + directed,
                    "m": random_sample, "rng": rng}
    results = []
    for test in range(tests):
        D, N, all_cliques = MV_greedy.prepare_data(N_CLASSES, N_NODES,
                                                   graph_config, rng,
                                                   decentralized_clique=
                                                   decentralized_clique,
                                                   shard_size=shard_size,
                                                   samples_per_class=samples_per_class,
                                                   n_shards_per_node=n_shards_per_node
                                                   )
        result_decentr = decentralized_algorithm(
            all_cliques, rng, D, directed=directed, iterations=iterations,
            random_sample=random_sample, max_n_nodes=max_n_nodes)
        results.append(result_decentr[1])
    results = np.array(results)
    x_range = range(1, iterations + 1)
    show_and_save = False
    if axs is None:
        show_and_save = True
        _, axs = plt.subplots()
    axs.grid(True)
    average = results[:, :, 1].mean(axis=0)
    axs.plot(x_range, average, c=color, label=legend_label)
    if plot_max_avg:
        axs.plot(x_range, results[:, :, 2].max(axis=0), c=color, alpha=0.5,
                 linestyle='--')
    if plot_min_avg:
        axs.plot(x_range, results[:, :, 0].min(axis=0), c=color, alpha=0.5,
                 linestyle='--')
    axs.set_xlabel("Number of iterations")
    axs.set_ylabel("Average skew")
    axs.set_title("Centralized vs decentralized greedy")
    if plot_std:
        std = results[:, :, 3].mean(axis=0)
        axs.fill_between(x_range, average - std, average + std, color=color,
                         alpha=0.3)
    if show_and_save:
        plt.savefig(
            f"../pictures/two_greedy_comparison_max_n_nodes_{str(max_n_nodes)}"
            f"n_nodes_{str(N_NODES)}.pdf", bbox_inches="tight")
        plt.show()
    else:
        axs.set_title(f"Max {str(max_n_nodes)} nodes")


def centr_greedy_skew_from_max_clique_size(N_NODES, max_clique_sizes,
                                           test_data, iterations=None,
                                           sharded=False):
    tests = len(test_data)
    results = np.zeros((len(max_clique_sizes), tests))
    for test in range(tests):
        N, all_nodes = test_data[test]
        n_classes = N.shape[1]
        D = greedy_funcs.get_global_distribution(N, n_classes)
        for i in range(len(max_clique_sizes)):
            result_centr = greedy_funcs.create_cliques_and_get_average_skew(
                N, D, max_n_nodes=max_clique_sizes[i])
            results[i][test] = result_centr[1][0][-1]
            # print(max_clique_sizes[i], results[i][test])
            print(i, test)
            # print(max_clique_sizes[i], MV_greedy.get_sizes_of_cliques(result_centr[0]))
    with open(f"../data/centr_greedy_skew_from_max_clique_size_n_nodes_"
              f"{str(N_NODES)}_tests_{str(tests)}_sharded_{str(sharded)}"
              f"_max_size_{str(max(max_clique_sizes))}",
              "wb") as file:
        pickle.dump(results, file)
    return results


def MV_greedy_skew_from_max_clique_size(N_NODES, max_clique_sizes, test_data,
                                        iterations=10, directed="",
                                        random_sample=10, sharded=False):
    tests = len(test_data)
    rng = np.random.default_rng(24)
    results = np.zeros((len(max_clique_sizes), tests))
    for test in range(tests):
        N, all_nodes = test_data[test]
        n_classes = N.shape[1]
        D, _, all_cliques = MV_greedy.create_cliques_and_compute_global_distribution(
            N, n_classes, all_nodes)
        for i in range(len(max_clique_sizes)):
            result_decentr = MV_greedy.decentralized_greedy_resolving_conflicts(
                copy.deepcopy(all_cliques), rng, D, directed=directed,
                iterations=iterations, random_sample=random_sample,
                max_n_nodes=max_clique_sizes[i])
            results[i][test] = result_decentr[1][-1][1]
            print(i, test)
            # print(max_clique_sizes[i], MV_greedy.get_sizes_of_cliques(result_decentr[0]))
    with open("../data/MV_greedy_skew_from_max_clique_size_n_nodes_" + str(
            N_NODES) + f"_tests_{str(tests)}_sharded_{str(sharded)}"
                       f"_max_size_{str(max(max_clique_sizes))}_iterations"
                       f"_{str(iterations)}", "wb") as file:
        pickle.dump(results, file)
    return results


def bipartite_greedy_skew_from_max_clique_size(N_NODES, max_clique_sizes,
                                               test_data, iterations=10,
                                               directed="", random_sample=10,
                                               sharded=False):
    tests = len(test_data)
    rng = np.random.default_rng(24)
    results = np.zeros((len(max_clique_sizes), tests))
    for test in range(tests):
        N, all_nodes = test_data[test]
        n_classes = N.shape[1]
        D, _, all_cliques = MV_greedy.create_cliques_and_compute_global_distribution(
            N, n_classes, all_nodes,
            decentralized_clique=bipartite_funcs.DecentralizedClique)
        for i in range(len(max_clique_sizes)):
            result_decentr = bipartite_funcs.decentralized_greedy_bipartite_solution(
                copy.deepcopy(all_cliques), rng, D, directed=directed,
                iterations=iterations, random_sample=random_sample,
                max_n_nodes=max_clique_sizes[i])
            results[i][test] = result_decentr[1][-1][1]
            print(i, test)
            # print(max_clique_sizes[i], MV_greedy.get_sizes_of_cliques(result_decentr[0]))
    filename = f"../data/bipartite_greedy_skew_from_max_clique_size_n_nodes_" \
               f"{str(N_NODES)}_tests_{str(tests)}_sharded_{str(sharded)}" \
               f"_max_size_{str(max(max_clique_sizes))}" \
               f"_iterations_{str(iterations)}"
    with open(filename, "wb") as file:
        pickle.dump(results, file)
    return results


def random_skew_from_max_clique_size(N_NODES, max_clique_sizes, test_data,
                                     iterations=None, sharded=False):
    tests = len(test_data)
    results = np.zeros((len(max_clique_sizes), tests))
    rng = np.random.default_rng(24)
    for test in range(tests):
        N, all_nodes = test_data[test]
        n_classes = N.shape[1]
        _, results[:, test], _, _, _ = random_alg.run_random_assignment(
            N_NODES, n_classes, rng, max_clique_sizes, test_data=N)
    with open(f"../data/random_skew_from_max_clique_size_n_nodes_"
              f"{str(N_NODES)}_tests_{str(tests)}_sharded_{str(sharded)}"
              f"_max_size_{str(max(max_clique_sizes))}",
              "wb") as file:
        pickle.dump(results, file)
    return results


def plot_decentr_centr_random(colormap, number_of_nodes=600,
                              sizes=(70, 60, 50, 40, 30, 25, 20, 15, 10, 5),
                              tests=3, directed="", random_sample=10,
                              N_CLASSES=10, n_shards_per_node=None,
                              samples_per_class=None, shard_size=None,
                              iterations=3, rerun_exps=False):
    names = ["centr_greedy",
             "random", "MV_greedy", "bipartite_greedy"
             ]
    funcs = [centr_greedy_skew_from_max_clique_size,
             random_skew_from_max_clique_size,
             MV_greedy_skew_from_max_clique_size,
             bipartite_greedy_skew_from_max_clique_size
             ]
    labels = ["Centralized greedy", "Random", "Cycle handling", "Bipartite"]
    assert len(names) == len(funcs)
    rng = np.random.default_rng(24)
    graph_config = {"n": number_of_nodes, "topology": "RANDOM" + directed,
                    "m": random_sample, "rng": rng}
    test_data = []
    sharded = (shard_size is not None)
    for test in range(tests):
        print("Creating test data", test)
        test_data.append(MV_greedy.create_nodes_distributions(N_CLASSES,
                                                              number_of_nodes,
                                                              graph_config,
                                              n_shards_per_node, rng,
                                              samples_per_class, shard_size))
    for i in range(len(names)):
        addition = ""
        if i > 1:
            addition = f"_iterations_{str(iterations)}"
        filename = f"../data/{names[i]}_skew_from_max_clique_size_n_nodes_" \
                   f"{str(number_of_nodes)}_tests_{str(tests)}_sharded_" \
                   f"{sharded}_max_size_{str(max(sizes))}" + addition
        if i == 1 or rerun_exps or not os.path.exists(filename):
            print(i)
            if i == 1:
                curr_sizes = range(min(sizes), max(sizes) + 1)
            else:
                curr_sizes = sizes
            results = funcs[i](number_of_nodes, curr_sizes, test_data,
                               iterations=iterations, sharded=sharded)
        else:
            with open(filename, "rb") as file:
                results = pickle.load(file)
        std = results.std(axis=1)
        mean_res = results.mean(axis=1)
        if i == 1:
            curr_sizes = range(min(sizes), max(sizes) + 1)
        else:
            curr_sizes = sizes
        plt.plot(curr_sizes, results.mean(axis=1), label=labels[i],
                 color=colormap[i])
        plt.fill_between(curr_sizes, mean_res - std, mean_res + std, alpha=0.3,
                         color=colormap[i])
    plt.grid(True)
    plt.legend(loc='best')
    plt.xlabel("Max number of nodes in a clique")
    plt.ylabel("Average skew")
    plt.title(f"{str(number_of_nodes)} nodes")
    plt.ylim(0, None)
    plt.savefig(
        f"../pictures/centr_vs_MV_{str(iterations)}_vs_random_vs_bipartite_"
        f"{str(iterations)}_from_max_node_in_a_clique_n_nodes_"
        f"{str(number_of_nodes)}_with_std_{str(max(sizes))}_sharded_"
        f"{shard_size is not None}.pdf",
        bbox_inches="tight")
    plt.show()


if __name__ == "__main__":
    COLORMAP = [constants.Figures.COLOR_PRIMARY,
                constants.Figures.COLOR_SECONDARY,
                constants.Figures.COLOR_TERTIARY,
                constants.Figures.COLOR_QUATERNARY]
    ITERATIONS = 30
    N_NODES = 600
    ALG_NAME = "bipartite_vs_MV"
    SIZES = {600: (420, 400, 350, 300, 250, 200, 150, 100,
                                     70, 60, 50, 40, 30, 25, 20, 15, 10, 5),
             100: (70, 60, 50, 40, 30, 25, 20, 15, 10, 5)}
    SEED = 25
    # 1
    # rng = np.random.default_rng(24)
    # centr_greedy_vs_MV(600, rng)

    # 2
    # print("HAHA")
    plot_decentr_centr_random(COLORMAP, number_of_nodes=N_NODES,
                              iterations=ITERATIONS, rerun_exps=False,
                              tests=constants.ExperimentSetting.TESTS,
                              shard_size=50,
                              samples_per_class=constants.ExperimentSetting.SAMPLES_PER_CLASS,
                              sizes=SIZES[N_NODES]
                              )

    # 3 -- 10 iterations are enough
    # rng = np.random.default_rng(SEED)
    # cliques_size_array = [[20, 30, 40], [50, 60, 70]]
    # fig, axs = plt.subplots(2, 3, figsize=(17, 10), sharey=True)
    # configuration = {"N_NODES": N_NODES, "rng": rng, "iterations": ITERATIONS,
    #                  "plot_max_avg": True, "plot_min_avg": True,
    #                  "shard_size": constants.ExperimentSetting.SHARD_SIZE,
    #                  "samples_per_class": constants.ExperimentSetting.SAMPLES_PER_CLASS}
    # for i in range(len(cliques_size_array)):
    #     for j in range(len(cliques_size_array[i])):
    #         configuration["axs"] = axs[i, j]
    #         configuration["max_n_nodes"] = cliques_size_array[i][j]
    #         configuration["decentralized_clique"] = bipartite_funcs.DecentralizedClique
    #         configuration["decentralized_algorithm"] = bipartite_funcs.decentralized_greedy_bipartite_solution
    #         configuration["color"] = "blue"
    #         configuration["legend_label"] = "Bipartite"
    #         plot_decentralized_evolution(**configuration)
    #         configuration["decentralized_clique"] = MV_greedy.DecentralizedClique
    #         configuration["decentralized_algorithm"] = MV_greedy.decentralized_greedy_resolving_conflicts
    #         configuration["color"] = "red"
    #         configuration["legend_label"] = "Cycle handling"
    #         plot_decentralized_evolution(**configuration)
    #         axs[i, j].set_ylim(bottom=0.0)
    # axs[0, 0].legend(loc='best')
    # plt.savefig(
    #     f"../pictures/centr_vs_greedy_{ALG_NAME}_different_clique_sizes_"
    #     f"{ITERATIONS}_iters_{SEED}_seed_sharded"
    #     f".pdf", bbox_inches='tight')
    # plt.show()
    #
    # 4
    # plot_decentr_centr_random(11, ra)
