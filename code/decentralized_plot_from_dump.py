import pickle
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path


def set_up_graph(axs, n_classes, i):
    first_index, second_index = i // 3, i % 3
    axs[first_index, second_index].grid(True)
    if n_classes > 1:
        axs[first_index, second_index].set_title(
            str(n_classes) + " classes")
    else:
        axs[first_index, second_index].set_title(
            str(n_classes) + " class")
    axs[first_index, second_index].set_xlabel("Number of iterations")
    return first_index, second_index


def transpose_to_get_curr_data(len_of_cycles, transpose=False):
    # BUT: len(classes_range) == 1 or len(random_sample) == 1!!!!
    if len_of_cycles.shape[0] == 1:
        curr_data = np.transpose(len_of_cycles, [1, 0] + list(range(
            2, len_of_cycles.ndim)))[:, 0]
    else:
        curr_data = len_of_cycles[:, 0]
    if transpose:
        curr_data = np.transpose(curr_data, [1, 0] + list(range(
            2, curr_data.ndim)))
    return curr_data


def plot_graph(axs, i, len_of_cycles, nodes_range, ylabel, index, legend,
               indices=None, transpose=False):
    curr_data = transpose_to_get_curr_data(len_of_cycles, transpose)
    # curr_data.shape == (len(classes_range/random_sample), len(nodes_range),
    # TESTS == 3, iterations == 10, 2, 3), -2 dimension is [len_of_cycle,
    # share_of_cycle], last dimension is [min, mean, max]
    axs.set_ylabel(ylabel)
    x_range = range(1, curr_data.shape[-3] + 1)
    if indices is None:
        indices = range(len(nodes_range))
    for j in indices:
        axs.plot(x_range,
                 curr_data[i, j, :, :, index, 1].mean(axis=0),
                 label=str(nodes_range[j]) + legend)


def plot_graph_for_small_shape(X_data, axs, i, legend, nodes_range,
                               indices=None, transpose=False):
    curr_data = transpose_to_get_curr_data(X_data, transpose)
    # curr_data.shape == (len(classes_range/random_samples), len(nodes_range),
    # TESTS == 3, iterations == 10)
    x_range = range(1, curr_data.shape[-1] + 1)
    if indices is None:
        indices = range(len(nodes_range))
    for j in indices:
        axs.plot(x_range, curr_data[i, j].mean(axis=0), label=str(
            nodes_range[j]) + legend)


def plot_graph_for_mean_time(X_data, axs, i, legend, nodes_range,
                             indices=None, variance=False, transpose=False):
    curr_data = transpose_to_get_curr_data(X_data, transpose)
    # curr_data.shape == (len(random_samples/classes_range), len(nodes_range),
    # TESTS == 3, iterations == 10, 3), last dimension == [min_size,
    # mean_size, max_size]
    x_range = range(1, curr_data.shape[-2] + 1)
    if indices is None:
        indices = range(len(nodes_range))
    for j in indices:
        average = curr_data[i, j, :, :, 1].mean(axis=0)
        if not variance:
            axs.plot(x_range, average, label=str(nodes_range[j]) +
                                                              legend)
        else:
            axs.plot(x_range, average,
                     label=str(nodes_range[j]) + legend, c='blue')
            axs.plot(x_range, curr_data[i, j, :, :, 2].max(axis=0), c='blue',
                     alpha=0.5)
            axs.plot(x_range, curr_data[i, j, :, :, 0].min(axis=0), c='blue',
                     alpha=0.5)
            if curr_data.shape[-1] > 3:
                std = curr_data[i, j, :, :, 3].mean(axis=0)
                axs.fill_between(x_range, average - std,
                                 average + std,
                                 color='#888888', alpha=0.5)


def subplot_vs_iterations(X, X_data, axs, i, nodes_range, share_of_cycles,
                          legend=" nodes", indices=None, variance=False,
                          transpose=False, top_ylim=None):
    if X == "number_of_components":
        axs.set_ylabel("Number of components")
        plot_graph_for_small_shape(X_data, axs, i,
                                   legend, nodes_range, indices, transpose=
                                   transpose)
    elif X == "size_of_components":
        axs.set_ylabel("Average size of components")
        plot_graph_for_mean_time(X_data, axs, i, legend, nodes_range,
                                 indices, transpose=transpose)
    elif X == "len_of_cycles" and not share_of_cycles:
        plot_graph(axs, i, X_data, nodes_range, "Average len of len_of_cycles",
                   0, legend, indices, transpose=transpose)
    elif X == "len_of_cycles" and share_of_cycles:
        plot_graph(axs, i, X_data, nodes_range,
                   "Average share of a cycle in a component", 1, legend,
                   indices, transpose=transpose)
        X = "share_of_cycles"
    elif X == "number_of_needed_iterations":
        axs.set_ylabel("Number of communication rounds for resolving\n"
                       "conflicts")
        plot_graph_for_small_shape(X_data, axs, i, legend, nodes_range,
                                   indices, transpose=transpose)
    elif X == "mean_time_for_iteration":
        axs.set_ylabel("Mean time for a communication round across\nall the "
                       "nodes in the system (sec)")
        plot_graph_for_mean_time(X_data, axs, i, legend, nodes_range,
                                 indices, transpose=transpose)
    elif X == "number_of_messages":
        axs.set_ylabel("Number of messages")
        plot_graph_for_small_shape(X_data, axs, i, legend, nodes_range,
                                   indices, transpose=transpose)
    elif X == "number_of_cliques":
        curr_data = transpose_to_get_curr_data(X_data, transpose=transpose)
        # curr_data.shape == (len(random_samples/classes_range),
        # len(nodes_range), TESTS == 3, iterations + 1 == 11); last dimension:
        # [before first iteration, all other measurements are after each of ten
        # iterations]
        axs.set_ylabel("Number of cliques")
        if indices is None:
            indices = range(len(nodes_range))
        for j in indices:
            axs.plot(curr_data[i, j, :, :].mean(axis=0),
                     label=str(nodes_range[j]) + legend)
    elif X == "average_skew_iterations":
        axs.set_ylabel("Average skew")
        plot_graph_for_mean_time(X_data, axs, i, legend, nodes_range,
                                 indices, variance, transpose=transpose)
        axs.set_ylim(0, top_ylim)
    elif X == "size_of_cliques":
        curr_data = transpose_to_get_curr_data(X_data, transpose=transpose)
        # curr_data.shape == (len(random_samples/classes_range),
        # len(nodes_range), TESTS == 3, iterations + 1 == 11, 4); -2 dimension:
        # [before first iteration, all other measurements are after each of ten
        # iterations], last dimension: [min, mean, max, std]
        axs.set_ylabel("Size of cliques")
        if indices is None:
            indices = range(len(nodes_range))
        for j in indices:
            # print(curr_data[i, j, 0, -1, 1])
            axs.plot(curr_data[i, j, 0, :, 1],
                     label=str(nodes_range[j]) + legend, c='blue')
            if variance:
                axs.plot(curr_data[i, j, 0, :, 2], c='blue', alpha=0.5)
                axs.plot(curr_data[i, j, 0, :, 0], c='blue', alpha=0.5)
                std = curr_data[i, j, 0, :, 3]
                axs.fill_between(range(curr_data.shape[-2]), curr_data[i, j,
                                                            0, :,
                                                  1] - std,
                                 curr_data[i, j, 0, :, 1] + std, alpha=0.5,
                                 color='#888888')
    return X


def plot_X_on_the_first_iteration(X, X_path=None, suffix="",
                                  share_of_cycles=False, show=True,
                                  classes_range=(10,),
                                  nodes_range=(20, 50, 75, 100, 250, 500, 750,
                                               1000, 1500),):
    if X_path is None:
        X_path = X
    with open("..//data//" + X_path + "//from_classes_iterations_1" + suffix,
              "rb") as file:
        X_data = pickle.load(file)
    plt.grid(True)
    curr_data = transpose_to_get_curr_data(X_data)
    if X == "number_of_components":
        # curr_data.shape == (len(classes_range) == 1, len(nodes_range) == 9,
        # TESTS == 3, 1); mean is taken by TESTS
        plt.plot(nodes_range, curr_data[0].mean(axis=1))
        plt.scatter(nodes_range, curr_data[0].mean(axis=1))
        plt.ylabel("Number of components on the first iteration")
    elif X == "size_of_components":
        # curr_data.shape == (len(classes_range) == 1, len(nodes_range) == 9,
        # TESTS == 3, 1, 3), last dimension == [min_size, mean_size, max_size]
        plt.plot(nodes_range, curr_data[0, :, :, :, 1].mean(axis=1))
        plt.scatter(nodes_range, curr_data[0, :, :, :, 1].mean(axis=1))
        plt.ylabel("Average size of components on the first iteration")
    elif X == "len_of_cycles" and not share_of_cycles:
        # curr_data.shape == (len(classes_range) == 1, len(nodes_range) == 9,
        # TESTS == 3, 1, 2, 3), -2 dimension is [len_of_cycle, share_of_cycle],
        # last dimension is [min, mean, max]
        plt.plot(nodes_range, curr_data[0, :, :, :, 0, 1].mean(axis=1))
        plt.scatter(nodes_range, curr_data[0, :, :, :, 0, 1].mean(axis=1))
        plt.ylabel("Average len of len_of_cycles on the first iteration")
    elif X == "len_of_cycles" and share_of_cycles:
        # curr_data.shape == (len(classes_range) == 1, len(nodes_range) == 9,
        # TESTS == 3, 1, 2, 3), -2 dimension is [len_of_cycle, share_of_cycle],
        # last dimension is [min, mean, max]
        plt.plot(nodes_range, curr_data[0, :, :, :, 1, 1].mean(axis=1))
        plt.scatter(nodes_range, curr_data[0, :, :, :, 1, 1].mean(axis=1))
        plt.ylabel("Average share of a cycle in a component on the first "
                   "iteration")
        X = "share_of_cycles"
    elif X == "number_of_needed_iterations":
        # curr_data.shape == (len(classes_range) == 1, len(nodes_range) == 9,
        # TESTS == 3, 1)
        plt.plot(nodes_range, curr_data[0].mean(axis=1))
        plt.scatter(nodes_range, curr_data[0].mean(axis=1))
        plt.ylabel("Number of communication rounds for resolving\nconflicts on"
                   " the first iteration")
    elif X == "mean_time_for_iteration":
        # curr_data.shape == (len(classes_range) == 1, len(nodes_range) == 9,
        # TESTS == 3, 1, 3), last dimension == [min_size, mean_size, max_size]
        plt.plot(nodes_range, curr_data[0, :, :, :, 1].mean(axis=1))
        plt.scatter(nodes_range, curr_data[0, :, :, :, 1].mean(axis=1))
        plt.ylabel("Mean time for a communication round across\nall the nodes "
                   "in the system (sec)")
    elif X == "number_of_messages":
        # curr_data.shape == (len(classes_range) == 1, len(nodes_range) == 9,
        # TESTS == 3, 1)
        plt.plot(nodes_range, curr_data[0].mean(axis=1))
        plt.scatter(nodes_range, curr_data[0].mean(axis=1))
        plt.ylabel("Number of messages on the first iteration")
    elif X == "number_of_cliques":
        # curr_data.shape == (len(classes_range) == 1, len(nodes_range) == 9,
        # TESTS == 3, 2); last dimension: [before first iteration, after
        # first iteration]
        plt.plot(nodes_range, curr_data[0, :, :, 1].mean(axis=1))
        plt.scatter(nodes_range, curr_data[0, :, :, 1].mean(axis=1))
        plt.ylabel("Number of cliques after the first iteration")
    elif X == "average_skew_iterations":
        # curr_data.shape == (len(classes_range) == 1, len(nodes_range) == 9,
        # TESTS == 3, 1, 3)
        plt.plot(nodes_range, curr_data[0, :, :, :, 1].mean(axis=1))
        plt.scatter(nodes_range, curr_data[0, :, :, :, 1].mean(axis=1))
        plt.ylabel("Average skew after the first iteration")
    elif X == "size_of_cliques":
        # curr_data.shape == (len(classes_range) == 1, len(nodes_range) == 9,
        # TESTS == 3, iterations + 1 == 2, 3); last dimension: [before first
        # iteration, all other measurements are after each of ten iterations],
        # last dimension: [min, mean, max]
        plt.plot(nodes_range, curr_data[0, :, :, 1, 1].mean(axis=1))
        plt.scatter(nodes_range, curr_data[0, :, :, 1, 1].mean(axis=1))
        plt.ylabel("Size of cliques after the first iteration")
    plt.xlabel("Number of nodes")
    plt.title("10 classes")
    parent_folder = "../pictures/" + X_path
    Path(parent_folder).mkdir(parents=True, exist_ok=True)
    filename = parent_folder + "/on_the_first_iteration" + suffix + ".pdf"
    plt.savefig(filename, bbox_inches="tight")
    if show:
        plt.show()
    else:
        plt.close()


def plot_X_for_random_samples_on_the_first_iteration(X, X_path=None, suffix="",
                                                     share_of_cycles=False,
                                                     show=True,
                                                     random_samples=(1, 2, 5,
                                                                     10, 19),
                                                     nodes_range=(20, 50, 80,
                                                                  100, 250,
                                                                  500, 750,
                                                                  1000, 1500)):
    if X_path is None:
        X_path = X
    with open("..//data//" + X_path + "//from_random_sample_iterations_1" +
              suffix, "rb") as file:
        X_data = pickle.load(file)
    curr_data = transpose_to_get_curr_data(X_data)
    plt.grid(True)
    if X == "number_of_components":
        # curr_data.shape == (len(random_samples) == 5, len(nodes_range) == 9,
        # TESTS == 3, 1); mean is taken by TESTS
        for i in range(len(random_samples)):
            plt.plot(nodes_range, curr_data[i].mean(axis=1),
                     label=str(random_samples[i]) + " random samples")
            plt.scatter(nodes_range, curr_data[i].mean(axis=1))
            # plt.fill_between(nodes_range, curr_data[i].min(axis=1)[:, 0],
            #                  curr_data[i].max(axis=1)[:, 0],
            #                  alpha=0.5)
        plt.ylabel("Number of components on the first iteration")
    elif X == "size_of_components":
        # curr_data.shape == (len(random_samples) == 5, len(nodes_range) == 9,
        # TESTS == 3, 1, 3), last dimension == [min_size, mean_size, max_size]
        for i in range(len(random_samples)):
            plt.plot(nodes_range, curr_data[i, :, :, :, 1].mean(axis=1),
                     label=str(random_samples[i]) + " random samples")
            plt.scatter(nodes_range, curr_data[i, :, :, :, 1].mean(axis=1))
        plt.ylabel("Average size of components on the first iteration")
    elif X == "len_of_cycles" and not share_of_cycles:
        # curr_data.shape == (len(random_samples) == 5, len(nodes_range) == 9,
        # TESTS == 3, 1, 2, 3), -2 dimension is [len_of_cycle, share_of_cycle],
        # last dimension is [min, mean, max]
        for i in range(len(random_samples)):
            plt.plot(nodes_range, curr_data[i, :, :, :, 0, 1].mean(axis=1),
                     label=str(random_samples[i]) + " random samples")
            plt.scatter(nodes_range, curr_data[i, :, :, :, 0, 1].mean(axis=1))
        plt.ylabel("Average len of len_of_cycles on the first iteration")
    elif X == "len_of_cycles" and share_of_cycles:
        # curr_data.shape == (len(random_samples) == 5, len(nodes_range) == 9,
        # TESTS == 3, 1, 2, 3), -2 dimension is [len_of_cycle, share_of_cycle],
        # last dimension is [min, mean, max]
        for i in range(len(random_samples)):
            plt.plot(nodes_range, curr_data[i, :, :, :, 1, 1].mean(axis=1),
                     label=str(random_samples[i]) + " random samples")
            plt.scatter(nodes_range, curr_data[i, :, :, :, 1, 1].mean(axis=1))
        plt.ylabel("Average share of a cycle in a component on the first "
                   "iteration")
        X = "share_of_cycles"
    elif X == "number_of_needed_iterations":
        # curr_data.shape == (len(random_samples) == 5, len(nodes_range) == 9,
        # TESTS == 3, 1)
        for i in range(len(random_samples)):
            plt.plot(nodes_range, curr_data[i].mean(axis=1),
                     label=str(random_samples[i]) + " random samples")
            plt.scatter(nodes_range, curr_data[i].mean(axis=1))
        plt.ylabel(
            "Number of communication rounds for resolving\nconflicts on"
            " the first iteration")
    elif X == "mean_time_for_iteration":
        # curr_data.shape == (len(random_samples) == 5, len(nodes_range) == 9,
        # TESTS == 3, 1, 3), last dimension == [min_size, mean_size, max_size]
        for i in range(len(random_samples)):
            plt.plot(nodes_range, curr_data[i, :, :, :, 1].mean(axis=1),
                     label=str(random_samples[i]) + " random samples")
            plt.scatter(nodes_range, curr_data[i, :, :, :, 1].mean(axis=1))
        plt.ylabel("Mean time for a communication round across\nall the nodes "
                   "in the system (sec)")
    elif X == "number_of_messages":
        # curr_data.shape == (len(random_samples) == 5, len(nodes_range) == 9,
        # TESTS == 3, 1)
        for i in range(len(random_samples)):
            plt.plot(nodes_range, curr_data[i].mean(axis=1),
                     label=str(random_samples[i]) + " random samples")
            plt.scatter(nodes_range, curr_data[i].mean(axis=1))
        plt.ylabel("Number of messages on the first iteration")
    elif X == "number_of_cliques":
        # curr_data.shape == (len(random_samples) == 5, len(nodes_range) == 9,
        # TESTS == 3, 2); last dimension: [before first iteration, after
        # first iteration]
        for i in range(len(random_samples)):
            plt.plot(nodes_range, curr_data[i, :, :, 1].mean(axis=1),
                     label=str(random_samples[i]) + " random samples")
            plt.scatter(nodes_range, curr_data[i, :, :, 1].mean(axis=1))
        plt.ylabel("Number of cliques after the first iteration")
    elif X == "average_skew_iterations":
        # curr_data.shape == (len(random_samples) == 5, len(nodes_range) == 9,
        # TESTS == 3, 1, 3)
        for i in range(len(random_samples)):
            plt.plot(nodes_range, curr_data[i, :, :, :, 1].mean(axis=1),
                     label=str(random_samples[i]) + " random samples")
            plt.scatter(nodes_range, curr_data[i, :, :, :, 1].mean(axis=1))
        plt.ylabel("Average skew after the first iteration")
    elif X == "size_of_cliques":
        # curr_data.shape == (len(random_samples) == 5, len(nodes_range) == 9,
        # TESTS == 3, iterations + 1 == 2, 3); last dimension: [before first
        # iteration, all other measurements are after each of ten iterations],
        # last dimension: [min, mean, max]
        for i in range(len(random_samples)):
            plt.plot(nodes_range, curr_data[i, :, :, 1, 1].mean(axis=1),
                     label=str(random_samples[i]) + " random samples")
            plt.scatter(nodes_range, curr_data[i, :, :, 1, 1].mean(axis=1))
        plt.ylabel("Size of cliques after the first iteration")
    plt.title("10 classes")
    plt.legend(loc='best')
    plt.xlabel("Number of nodes")
    parent_folder = "../pictures/" + X_path
    Path(parent_folder).mkdir(parents=True, exist_ok=True)
    filename = parent_folder + "/for_random_samples_on_the_first_iteration" + \
               suffix + ".pdf"
    plt.savefig(filename, bbox_inches="tight")
    if show:
        plt.show()
    else:
        plt.close()


def plot_X_vs_iterations(X, X_path=None, suffix="", share_of_cycles=False,
                         show=True, classes_range=(1, 2, 5, 10, 100, 1000),
                         nodes_range = (50, 100, 600, 1000)):
    if X_path is None:
        X_path = X
    with open("..//data//" + X_path + "//from_classes_iterations_10" + suffix,
              "rb") as file:
        X_data = pickle.load(file)
    fig, axs = plt.subplots(2, 3, figsize=(15, 10), sharey=True)
    for i in range(len(classes_range)):
        first_index, second_index = set_up_graph(axs, classes_range[i], i)
        tmp = subplot_vs_iterations(X, X_data, axs[first_index, second_index],
                                    i, nodes_range, share_of_cycles)
        if i + 1 == len(classes_range):
            X = tmp
    axs[0, 0].legend(loc='best')
    parent_folder = "../pictures/" + X_path
    Path(parent_folder).mkdir(parents=True, exist_ok=True)
    filename = parent_folder + "/vs_iterations" + suffix + ".pdf"
    plt.savefig(filename, bbox_inches="tight")
    if show:
        plt.show()
    else:
        plt.close(fig)


def plot_X_vs_iterations_for_random_samples(X, X_path=None, suffix="",
                                            share_of_cycles=False, show=True,
                                            random_samples=(2, 6, 10, 18),
                                            nodes_range=(20, 50, 80, 100, 250,
                                                         500, 750, 1000, 1500)
                                            ):
    if X_path is None:
        X_path = X
    with open("..//data//" + X_path + "//from_random_sample_iterations_10" +
              suffix, "rb") as file:
        X_data = pickle.load(file)
    fig, axs = plt.subplots(2, 2, figsize=(15, 10), sharey=True)
    for i in range(len(random_samples)):
        first_index, second_index = i // 2, i % 2
        axs[first_index, second_index].grid(True)
        axs[first_index, second_index].set_title("Random sample of " + str(
            random_samples[i]))
        axs[first_index, second_index].set_xlabel("Number of iterations")
        tmp = subplot_vs_iterations(X, X_data, axs[first_index, second_index],
                                    i, nodes_range, share_of_cycles)
        if i + 1 == len(random_samples):
            X = tmp
    axs[0, 0].legend(loc='best')
    parent_folder = "../pictures/" + X_path
    Path(parent_folder).mkdir(parents=True, exist_ok=True)
    filename = parent_folder + "/vs_iterations_for_random_samples" + suffix + \
               ".pdf"
    plt.savefig(filename, bbox_inches="tight")
    if show:
        plt.show()
    else:
        plt.close(fig)


def plot_X_vs_iterations_for_number_of_nodes(X, X_path=None, suffix="",
                                             share_of_cycles=False, show=True,
                                             random_samples=(2, 6, 10, 18),
                                             nodes_range=(20, 50, 80, 100, 250,
                                                          500, 750, 1000,
                                                          1500),
                                             chosen_nodes_range_indices=(3, 5,
                                                                         7, 8)
                                             ):
    if X_path is None:
        X_path = X
    with open("..//data//" + X_path + "//from_random_sample_iterations_10" +
              suffix, "rb") as file:
        X_data = pickle.load(file)
    fig, axs = plt.subplots(2, 2, figsize=(15, 10), sharey=True)
    for i in range(len(chosen_nodes_range_indices)):
        first_index, second_index = i // 2, i % 2
        axs[first_index, second_index].grid(True)
        axs[first_index, second_index].set_title(str(
            nodes_range[chosen_nodes_range_indices[i]]) + " nodes")
        axs[first_index, second_index].set_xlabel("Number of iterations")
        tmp = subplot_vs_iterations(X, X_data,
                                    axs[first_index, second_index],
                                    chosen_nodes_range_indices[i],
                                    random_samples, share_of_cycles,
                                    legend=" random samples",
                                    transpose=True)
        if i + 1 == len(random_samples):
            X = tmp
    axs[0, 0].legend(loc='best')
    parent_folder = "../pictures/" + X_path
    Path(parent_folder).mkdir(parents=True, exist_ok=True)
    filename = parent_folder + "/vs_iterations_for_number_of_nodes" + suffix \
               + ".pdf"
    plt.savefig(filename, bbox_inches="tight")
    if show:
        plt.show()
    else:
        plt.close(fig)


def plot_X_vs_iterations_one_class(X, X_path=None, suffix="",
                                   share_of_cycles=False, show=True,
                                   classes_range=(1, 2, 5, 10, 100, 1000),
                                   nodes_range=(50, 100, 600, 1000),
                                   chosen_classes_range_index=3,
                                   chosen_nodes_range_indices=(2,),
                                   iterations=10, top_ylim=None,
                                   sharded=False):
    """
    Plot only one statistic for only one number of classes and random samples
        vs iterations.
    :param X: the name of the statistic
    :param suffix: directed or not
    :param share_of_cycles: if X == "len_of_cycles", shows if plotting len of
        len_of_cycles of share of len_of_cycles
    :param show: show the resulting graph or only save it
    :param classes_range: classes range (only one is chosen from it)
    :param nodes_range: nodes range (only one number of nodes is chosen from
        it)
    :param chosen_classes_range_index: index of the chosen number of classes
    :param chosen_nodes_range_indices: indices of the chosen numbers of nodes
    """
    if X_path is None:
        X_path = X
    with open(f"..//data//{X_path}//from_classes_iterations_"
              f"{str(iterations)}{suffix}_sharded_{str(sharded)}",
              "rb") as file:
        X_data = pickle.load(file)
    fig, axs = plt.subplots()
    axs.grid(True)
    n_classes = classes_range[chosen_classes_range_index]
    # if n_classes > 1:
    #     axs.set_title(str(n_classes) + " classes")
    # else:
    #     axs.set_title(str(n_classes) + " class")
    # axs.set_title(title)
    axs.set_xlabel("Number of iterations")
    subplot_vs_iterations(X, X_data, axs, chosen_classes_range_index,
                          nodes_range, share_of_cycles,
                          indices=chosen_nodes_range_indices,
                          variance=True, top_ylim=top_ylim)
    # plt.legend(loc='best')
    parent_folder = "../pictures/" + X_path
    Path(parent_folder).mkdir(parents=True, exist_ok=True)
    filename = parent_folder + f"/vs_{iterations}_iterations_" \
                               f"{classes_range[chosen_classes_range_index]}_classes_" + \
               f"{str(len(chosen_nodes_range_indices))}_nodes_variants{suffix}" \
               f"_sharded_{str(sharded)}.pdf"
    plt.savefig(filename, bbox_inches="tight")
    if show:
        plt.show()
    else:
        plt.close(fig)


def plot_max_number_of_comp_vs_classes(X_data, classes_range, nodes_range):
    curr_data = transpose_to_get_curr_data(X_data)
    # curr_data.shape == (len(classes_range) == 10, len(nodes_range) == 2,
    # TESTS == 3, 10); mean is taken by TESTS
    for i in range(len(nodes_range)):
        plt.plot(classes_range, curr_data[:, i].max(axis=(1, 2)),
                 label=str(nodes_range[i]) + " nodes")
        plt.scatter(classes_range, curr_data[:, i].max(axis=(1, 2)))


def plot_max_size_of_comp_vs_classes(X_data, classes_range, nodes_range):
    curr_data = transpose_to_get_curr_data(X_data)
    # curr_data.shape == (len(classes_range) == 10, len(nodes_range) == 2,
    # TESTS == 3, 10, 3), last dimension == [min_size, mean_size, max_size]
    for i in range(len(nodes_range)):
        plt.plot(classes_range, curr_data[:, i, :, -1, 1].mean(axis=1),
                 label=str(nodes_range[i]) + " nodes")
        plt.scatter(classes_range, curr_data[:, i, :, -1, 1].mean(axis=1))


def plot_X_vs_classes(X, X_path=None, suffix="", share_of_cycles=False,
                      show=True, classes_range=(2, 3, 4, 5, 6, 10, 50, 100,
                                                500, 1000),
                      nodes_range=(100, 1000)):
    if X_path is None:
        X_path = X
    with open("..//data//" + X_path + "//max_from_classes_iterations_10" +
              suffix, "rb") as file:
        X_data = pickle.load(file)
    plt.grid(True)
    if X == "number_of_components":
        plot_max_number_of_comp_vs_classes(X_data, classes_range, nodes_range)
        plt.ylabel("Maximum number of components across 10 iterations")
    elif X == "size_of_components":
        plot_max_size_of_comp_vs_classes(X_data, classes_range, nodes_range)
        plt.ylabel("Maximum size of components across 10 iterations")
    elif X == "len_of_cycles" and not share_of_cycles:
        curr_data = transpose_to_get_curr_data(X_data)
        # X_data.shape == (len(classes_range) == 10, len(nodes_range) == 2,
        # TESTS == 3, 10, 2, 3), -2 dimension is [len_of_cycle,
        # share_of_cycle], last dimension is [min, mean, max]
        for i in range(len(nodes_range)):
            plt.plot(classes_range, curr_data[:, i, :, :, 0, 2].max(axis=(1,
                                                                          2)),
                     label=str(nodes_range[i]) + " nodes")
            plt.scatter(classes_range, curr_data[:, i, :, :, 0, 2].max(axis=(
                1, 2)))
        plt.ylabel("Maximum len of len_of_cycles across 10 iterations")
    elif X == "len_of_cycles" and share_of_cycles:
        curr_data = transpose_to_get_curr_data(X_data)
        # curr_data.shape == (len(classes_range) == 10, len(nodes_range) == 2,
        # TESTS == 3, 10, 2, 3), -2 dimension is [len_of_cycle,
        # share_of_cycle], last dimension is [min, mean, max]
        for i in range(len(nodes_range)):
            plt.plot(classes_range, curr_data[:, i, :, :, 1, 2].max(axis=(1, 2
                                                                          )),
                     label=str(nodes_range[i]) + " nodes")
            plt.scatter(classes_range, curr_data[:, i, :, :, 1, 2].max(axis=(1,
                                                                          2)))
        plt.ylabel("Maximum share of a cycle in a component across 10 "
                   "iterations")
        X = "share_of_cycles"
    elif X == "number_of_needed_iterations":
        plot_max_number_of_comp_vs_classes(X_data, classes_range, nodes_range)
        plt.ylabel(
            "Maximum number of communication rounds for resolving\nconflicts "
            "across 10 iterations")
    elif X == "mean_time_for_iteration":
        plot_max_size_of_comp_vs_classes(X_data, classes_range, nodes_range)
        plt.ylabel("Max time for a communication round across\nall the nodes "
                   "in the system (sec)")
    elif X == "number_of_messages":
        plot_max_number_of_comp_vs_classes(X_data, classes_range, nodes_range)
        plt.ylabel("Maximum number of messages across 10 iterations")
    elif X == "number_of_cliques":
        plot_max_number_of_comp_vs_classes(X_data, classes_range, nodes_range)
        plt.ylabel("Maximum number of cliques across 10 iterations")
    elif X == "average_skew_iterations":
        plot_max_size_of_comp_vs_classes(X_data, classes_range, nodes_range)
        plt.ylabel("Average skew after 10 iterations")
        plt.ylim(bottom=0)
    elif X == "size_of_cliques":
        plot_max_size_of_comp_vs_classes(X_data, classes_range, nodes_range)
        plt.ylabel("Maximum size of cliques across 10 iterations")
    # plt.title("10 classes")
    plt.legend(loc='best')
    plt.xlabel("Number of classes")
    parent_folder = "../pictures/" + X_path
    Path(parent_folder).mkdir(parents=True, exist_ok=True)
    filename = parent_folder + "/vs_classes" + suffix + ".pdf"
    plt.savefig(filename, bbox_inches="tight")
    if show:
        plt.show()
    else:
        plt.close()


if __name__ == "__main__":
    funcs = [
        # plot_X_on_the_first_iteration,
        # plot_X_for_random_samples_on_the_first_iteration,
        # plot_X_vs_iterations,
        # plot_X_vs_iterations_for_random_samples,
        # plot_X_vs_iterations_for_number_of_nodes,
        plot_X_vs_iterations_one_class,
        # plot_X_vs_classes
    ]
    # ranges = [
    #     {"classes_range": [10],
    #      "nodes_range": [20, 50]},
    #     {"random_samples": [1, 2],
    #      "nodes_range": [20, 50]},
    #     {"classes_range": [1, 2],
    #      "nodes_range": [50, 100]},
    #     {"random_samples": [2, 6],
    #      "nodes_range": [20, 50]},
    #     {"random_samples": [2, 6],
    #      "nodes_range": [20, 50],
    #      "chosen_nodes_range_indices": [0, 1]},
    #     {"classes_range": [1, 2],
    #      "nodes_range": [50, 100],
    #      "chosen_classes_range_index": 0,
    #      "chosen_nodes_range_indices": (0,)},
    #     {"classes_range": [2, 3],
    #      "nodes_range": [100, 1000]}
    # ]
    # funcs = [plot_X_vs_classes]
    parameters = [
        # {"X": "number_of_components",
        #            "X_path": "MV/number_of_components", "show": False},
        #           {"X": "size_of_components", "X_path": "MV/size_of_components",
        #            "show": False},
        #           {"X": "len_of_cycles", "X_path": "MV/len_of_cycles",
        #            "share_of_cycles": False, "show": False},
        #           {"X": "len_of_cycles", "X_path": "MV/len_of_cycles",
        #            "share_of_cycles": True, "show": False},
        #           {"X": "number_of_needed_iterations",
        #            "X_path": "MV/number_of_needed_iterations", "show": False},
        #           {"X": "mean_time_for_iteration",
        #            "X_path": "MV/mean_time_for_iteration", "show": False},
        #           {"X": "number_of_messages", "X_path": "MV/number_of_messages",
        #            "show": False},
        #           {"X": "number_of_cliques", "X_path": "MV/number_of_cliques",
        #            "show": False},
                  {"X": "average_skew_iterations",
                   "X_path": "test_average_skew_iterations",
                   "show": False},
                  # {"X": "size_of_cliques", "X_path": "MV/size_of_cliques",
                  #  "show": False}
    ]
    # parameters = [{"X": "number_of_components", "show": False},
    #               {"X": "size_of_components", "show": False},
    #               {"X": "len_of_cycles", "share_of_cycles": False, "show": False},
    #               {"X": "len_of_cycles", "share_of_cycles": True, "show": False},
    #               {"X": "number_of_needed_iterations", "show": False},
    #               {"X": "mean_time_for_iteration", "show": False},
    #               {"X": "number_of_messages", "show": False},
    #               {"X": "number_of_cliques", "show": False},
    #               {"X": "average_skew_iterations", "show": False},
    #               {"X": "size_of_cliques", "show": False}]
    # parameters = [{"X": "number_of_cliques",
    #                "X_path": "bipartite/number_of_cliques", "show": False},
    #               {"X": "average_skew_iterations",
    #                "X_path": "bipartite/average_skew_iterations", "show": False},
    #               {"X": "size_of_cliques",
    #                "X_path": "bipartite/size_of_cliques", "show": False}]
    # for i in range(len(funcs)):
    #     for j in range(len(parameters)):
    #         curr_parameters = {**parameters[j], "suffix": ""}
    #         funcs[i](**curr_parameters)
    #         curr_parameters["suffix"] = "_DIRECTED"
    #         funcs[i](**curr_parameters)
    # for j in range(len(parameters)):
    #     curr_parameters = {**parameters[j], "suffix": "",
    #                        "chosen_classes_range_index": 3,
    #                        "chosen_nodes_range_indices": (0, 1, 2, 3)}
    #     plot_X_vs_iterations_one_class(**curr_parameters)
    #     curr_parameters["suffix"] = "_DIRECTED"
    #     plot_X_vs_iterations_one_class(**curr_parameters)
    for j in range(len(parameters)):
        curr_parameters = {**parameters[j], "classes_range": [10],
         "nodes_range": [100],
         "chosen_classes_range_index": 0,
         "chosen_nodes_range_indices": (0,),
                           "top_ylim": None,
                           "sharded": True}
        plot_X_vs_iterations_one_class(**curr_parameters)
        curr_parameters["suffix"] = "_DIRECTED"
        plot_X_vs_iterations_one_class(**curr_parameters)
