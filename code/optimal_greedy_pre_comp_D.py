import copy
from pathlib import Path
import pickle

import numpy as np
import matplotlib.pyplot as plt

import utils as utils
import functions
import constants


class OAClique:
    def __init__(self, node_distribution):
        self.all_nodes = [node_distribution]
        self.clique_distribution = np.array(self.all_nodes[-1], dtype=object)

    def update_clique_distribution(self):
        self.clique_distribution += self.all_nodes[-1]

    def add_node(self, node_distribution):
        self.all_nodes.append(node_distribution)
        self.update_clique_distribution()
        return self

    def get_clique_distribution(self):
        return self.clique_distribution

    def get_n_nodes_in_clique(self):
        return len(self.all_nodes)


def are_list_equal(list1, list2):
    new_list1 = []
    new_list2 = []

    for array1 in list1:
        sum1 = 0
        for sub_array1 in array1:
            sum1 += sum(sub_array1)
        new_list1.append(sum1)

    if list2 is None:
        return False, np.argsort(new_list1)

    for array2 in list2:
        sum2 = 0
        for sub_array2 in array2:
            sum2 += sum(sub_array2)
        new_list2.append(sum2)

    if sorted(new_list1) != sorted(new_list2):
        return False, np.argsort(new_list1)
    else:
        return True, np.argsort(new_list1)


def get_dc_combinations(n, k):
    if n.any():
        prev = None
        for t in get_dc_combinations(n[1:], k):
            are_list_equal_condition, argsort_values = are_list_equal(t, prev)
            tup = [list(array)
                   for array
                   in np.array(t, dtype=object)[argsort_values]]
            if not are_list_equal_condition:
                prev = tup
                for index in range(k):
                    yield tup[:index] + [[n[0]] + tup[index], ] + \
                          tup[index + 1:]
    else:
        yield [[] for _ in range(k)]


def get_ne_dc_combinations(n, k, d=None, constraint=None, return_best=False):
    """
    :param n: list of nodes
    :param k: number of cliques to construct
    :param d: global distribution
    :param constraint: max number of nodes per clique
    :param return_best: boolean
    :return: non-empty distributed cliques
    """
    best_dc = None
    best_avg_skew = 10
    for distributed_cliques in get_dc_combinations(n, k):
        if all(x for x in distributed_cliques):
            dc = []
            n_nodes_below_max = True
            cliques_skew = []
            for clique in distributed_cliques:
                c = None
                for index, node in enumerate(clique):
                    if index == 0:
                        c = OAClique(node)
                    else:
                        c.add_node(node)
                dc.append(c)
                if d is not None and return_best:
                    cliques_skew.append(utils.skew(c, d))
                if constraint:
                    if c.get_n_nodes_in_clique() > constraint:
                        n_nodes_below_max = False
            if return_best:
                current_avg_skew = np.array(cliques_skew).mean()
                if current_avg_skew < best_avg_skew:
                    if not constraint or n_nodes_below_max:
                        best_dc = dc
                        best_avg_skew = current_avg_skew
                del dc
            else:
                if (constraint and n_nodes_below_max) or not constraint:
                    yield dc
                else:
                    del dc
    if return_best:
        yield best_dc


def k_among_n(n, k):
    res = 1
    for x in range(n - k, n):
        res *= x + 1
    for x in range(k):
        res /= x + 1
    return res


def stirling_number_second_kind(n, k):
    res = 0
    for j in range(k + 1):
        res += (-1) ** (k - j) * k_among_n(k, j) * j ** n
    for x in range(k):
        res /= x + 1
    return res


def optimal_greedy(n, d, k_range, max_nodes_per_clique):
    dc_per_constraints = []
    dc_skew_list_statistics = [[], [], []]

    for index in range(len(max_nodes_per_clique)):
        best_dc = None
        best_avg_skew = 10
        min_skew = None
        max_skew = None
        for k in k_range:
            ne_dc_combinations = get_ne_dc_combinations(
                copy.deepcopy(n), k, constraint=max_nodes_per_clique[index],
                d=D, return_best=True
            )
            for distributed_cliques in ne_dc_combinations:
                if distributed_cliques is not None:
                    distributed_cliques_skew_list_temporal = []
                    for clique_dc in distributed_cliques:
                        distributed_cliques_skew_list_temporal.append(
                            utils.skew(clique_dc, d))
                    current_avg_skew = np.array(
                        distributed_cliques_skew_list_temporal).mean()
                    if current_avg_skew < best_avg_skew:
                        best_dc = distributed_cliques
                        best_avg_skew = current_avg_skew
                        min_skew = np.min(np.array(
                            distributed_cliques_skew_list_temporal))
                        max_skew = np.max(np.array(
                            distributed_cliques_skew_list_temporal))
                    else:
                        del distributed_cliques
                else:
                    break
        if best_dc:
            dc_per_constraints.append(best_dc)
            dc_skew_list_statistics[0].append(best_avg_skew)
            dc_skew_list_statistics[1].append(max_skew)
            dc_skew_list_statistics[2].append(min_skew)

    return max_nodes_per_clique, dc_skew_list_statistics


if __name__ == "__main__":
    PLOT_NAME = "skew_size_of_clique_optimal"
    COLORMAP = {
        (constants.ExperimentSetting.N_NODES,
         constants.ExperimentSetting.N_CLASSES
         ): constants.Figures.COLOR_PRIMARY
    }
    RANDOM_GENERATOR = np.random.default_rng(69)
    PICTURES_FOLDER_PATH = \
        "../pictures/optimal_solution/average_skew_iterations/"

    N = None
    if constants.ExperimentSetting.DATA_PARTITION_MODE == "simple":
        N = utils.create_nodes(RANDOM_GENERATOR,
                               constants.ExperimentSetting.N_CLASSES,
                               constants.ExperimentSetting.N_NODES)
    elif constants.ExperimentSetting.DATA_PARTITION_MODE == "shards":
        N = functions.data_partition(
            constants.ExperimentSetting.N_NODES,
            constants.ExperimentSetting.N_CLASSES,
            RANDOM_GENERATOR,
            constants.ExperimentSetting.SHARD_SIZE,
            constants.ExperimentSetting.SAMPLES_PER_CLASS)
    assert N is not None, "N has not been defined, check your partition " \
                          "mode, it should be simple or shards"
    D = utils.get_global_distribution(N,
                                      constants.ExperimentSetting.N_CLASSES)

    # dc_per_constraint_skew_statistics: 0.avg skew, 1.max skew, 2.min skew
    dc_per_constraint, dc_per_constraint_skew_statistics = optimal_greedy(
        N, D, constants.Constraints.K,
        constants.Constraints.MAX_NODES_PER_CLIQUE_OPTIMAL)

    # Save optimal_greedy results in a pickle file
    DATA_PATH = "../data/optimal_solution/"
    Path(DATA_PATH).mkdir(parents=True, exist_ok=True)
    with open(f"{DATA_PATH}dc_per_constraint_and_skew_statistics_"
              f"{constants.ExperimentSetting.DATA_PARTITION_MODE}_partition_"
              f"{constants.ExperimentSetting.N_NODES}_nodes.pickle",
              "wb") as file:
        pickle.dump((dc_per_constraint, dc_per_constraint_skew_statistics),
                    file)

    plt.xlabel("Max number of nodes in a clique")
    plt.ylabel("Average skew")
    plt.plot(constants.Constraints.MAX_NODES_PER_CLIQUE_OPTIMAL,
             dc_per_constraint_skew_statistics[0],
             color=constants.Figures.COLOR_PRIMARY)
    plt.plot(constants.Constraints.MAX_NODES_PER_CLIQUE_OPTIMAL,
             dc_per_constraint_skew_statistics[1],
             color=constants.Figures.COLOR_PRIMARY, alpha=0.4)
    plt.plot(constants.Constraints.MAX_NODES_PER_CLIQUE_OPTIMAL,
             dc_per_constraint_skew_statistics[2],
             color=constants.Figures.COLOR_PRIMARY, alpha=0.4)
    plt.grid(True)
    if constants.Figures.SAVE_PLOT:
        # Create folder if it does not exist
        Path(PICTURES_FOLDER_PATH).mkdir(parents=True, exist_ok=True)
        plt.savefig(f"{PICTURES_FOLDER_PATH}{PLOT_NAME}_"
                    f"{constants.ExperimentSetting.N_NODES}_"
                    f"{constants.ExperimentSetting.DATA_PARTITION_MODE}_"
                    f"partition.pdf", bbox_inches='tight')
    plt.show()
