import numpy as np
import matplotlib.pyplot as plt

CLASSES = 10
UNIFORM_DISTRIBUTION = np.ones(CLASSES) / CLASSES
rng = np.random.default_rng(0)
LEFT = 10
RIGHT = 2000
nodes_range = range(LEFT, RIGHT + 1, 10)
TESTS = 100
skew = np.zeros((TESTS, len(nodes_range)))

for i in range(TESTS):
    for j in range(len(nodes_range)):
        N = nodes_range[j]
        distribution = rng.uniform(100, 10001, (N, CLASSES)).sum(axis=0)
        distribution /= distribution.sum()
        skew[i][j] = np.abs(distribution - UNIFORM_DISTRIBUTION).sum()

plt.grid(True)
plt.plot(nodes_range, skew.mean(axis=0), c='blue')
plt.plot(nodes_range, skew.min(axis=0), alpha=0.4, c='blue')
plt.plot(nodes_range, skew.max(axis=0), alpha=0.4, c='blue')
plt.xlabel("Number of nodes")
plt.ylabel("Skew")
plt.savefig("../pictures/distribution_many_nodes.pdf", bbox_inches='tight')
plt.show()
