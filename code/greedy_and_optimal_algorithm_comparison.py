import numpy as np
import time
import copy

import greedy_with_pre_comp_of_D as ga
import optimal_greedy_pre_comp_D as oa
import utils

if __name__ == "__main__":
    N_CLASSES = 10
    N_NODES_LIST = range(3, 15)
    PICTURES_FOLDER_PATH = "../../test_pictures/"
    PLOT = True
    SAVE_PLOTS = False
    ITERATIONS = 1
    # K = range(2, N_NODES+1)

    GA_conf_skew_distribution_list = []
    OA_conf_skew_distribution_list = []
    GA_time = [[] for _ in N_NODES_LIST]
    OA_time = [[] for _ in N_NODES_LIST]
    GA_avg_skew_cliques = [[] for _ in N_NODES_LIST]
    OA_avg_skew_cliques = [[] for _ in N_NODES_LIST]
    OA_number_combinations = [[] for _ in N_NODES_LIST]

    GA_cliques_skew_list_individual_n_nodes_plot_info = [[] for _ in
                                                         N_NODES_LIST]
    # GA_DC_individual_n_nodes_plot_info = [[] for _ in N_NODES_LIST]
    GA_avg_skew_evolution_individual_n_nodes_plot_info = [[] for _ in
                                                          N_NODES_LIST]
    GA_max_skew_evolution_individual_n_nodes_plot_info = [[] for _ in
                                                          N_NODES_LIST]
    GA_min_skew_evolution_individual_n_nodes_plot_info = [[] for _ in
                                                          N_NODES_LIST]
    # GA_nodes_incidence_n_nodes_list_individual_n_nodes_plot_info = [[] for _ in N_NODES_LIST]
    # GA_average_skew_list_individual_n_nodes_plot_info = [[] for _ in N_NODES_LIST]
    # GA_max_skew_list_individual_n_nodes_plot_info = [[] for _ in N_NODES_LIST]
    # GA_min_skew_list_individual_n_nodes_plot_info = [[] for _ in N_NODES_LIST]
    OA_cliques_skew_list_individual_n_nodes_plot_info = [[] for _ in
                                                         N_NODES_LIST]
    for i, N_NODES in enumerate(N_NODES_LIST):
        RANDOM_GENERATOR = np.random.default_rng(69)
        for _ in range(ITERATIONS):
            N = utils.create_nodes(RANDOM_GENERATOR, N_CLASSES, N_NODES)
            D = utils.get_global_distribution(N, N_CLASSES)
            start_time_ga = time.time()
            GA_DC, skew_iterations_results = ga.create_cliques_and_get_skew_statistics(
                copy.deepcopy(N), D)
            end_time_ga = time.time() - start_time_ga
            GA_time[i].append(end_time_ga)

            K = [len(GA_DC)]

            GA_average_skew_list, GA_max_skew_list, GA_min_skew_list = skew_iterations_results

            n_nodes_and_skew_dict = dict()
            GA_cliques_skew_list = []
            print(f"GA_DC: {GA_DC[0].get_clique_distribution()}")
            for _, C in enumerate(GA_DC):
                n_nodes = len(C.all_nodes)
                C_skew = utils.skew(C, D)
                GA_cliques_skew_list.append(C_skew)

                if str(n_nodes) not in n_nodes_and_skew_dict:
                    n_nodes_and_skew_dict[str(n_nodes)] = [C_skew]
                else:
                    n_nodes_and_skew_dict[str(n_nodes)].append(C_skew)

            GA_n_nodes_list = np.array(
                [int(number) for number in list(n_nodes_and_skew_dict.keys())])
            GA_skew_average_of_cliques_with_same_n_nodes_list = []
            GA_skew_max = []
            GA_skew_min = []
            for skew_array in n_nodes_and_skew_dict.values():
                GA_skew_average_of_cliques_with_same_n_nodes_list.append(
                    np.array(skew_array).mean())
                GA_skew_max.append(max(skew_array))
                GA_skew_min.append(min(skew_array))
            GA_sorted_n_nodes_index = np.argsort(GA_n_nodes_list)

            GA_avg_skew_cliques[i].append(
                np.array(GA_cliques_skew_list).mean()
            )

            GA_cliques_skew_list_individual_n_nodes_plot_info[i].append(
                GA_cliques_skew_list)

            # GA_DC_individual_n_nodes_plot_info[i].append(GA_DC)

            GA_avg_skew_evolution_individual_n_nodes_plot_info[i].append(
                GA_average_skew_list)
            GA_max_skew_evolution_individual_n_nodes_plot_info[i].append(
                GA_max_skew_list)
            GA_min_skew_evolution_individual_n_nodes_plot_info[i].append(
                GA_min_skew_list)
            '''
            GA_nodes_incidence_n_nodes_list_individual_n_nodes_plot_info[
                i].append(GA_n_nodes_list[GA_sorted_n_nodes_index])
            GA_average_skew_list_individual_n_nodes_plot_info[i].append(
                np.array(GA_skew_average_of_cliques_with_same_n_nodes_list)[
                    GA_sorted_n_nodes_index])
            GA_max_skew_list_individual_n_nodes_plot_info[i].append(np.array(
                GA_skew_max)[GA_sorted_n_nodes_index])
            GA_min_skew_list_individual_n_nodes_plot_info[i].append(np.array(
                GA_skew_min)[GA_sorted_n_nodes_index])
            '''
            '''OPTIMAL'''

            combinations_distributed_cliques = []
            stirling_sum = 0
            start_time_oa = time.time()
            for k in K:
                combinations_distributed_cliques.extend([_ for _ in
                                                         oa.get_ne_dc_combinations(
                                                             copy.deepcopy(N),
                                                             k)])
                stirling_sum += oa.stirling_number_second_kind(len(N), k=k)
            end_time_oa = time.time() - start_time_oa
            OA_number_combinations[i].append(stirling_sum)
            OA_time[i].append(end_time_oa)

            combinations_distributed_cliques_avg_skew = []
            combinations_distributed_cliques_skew = []
            for distributed_cliques in combinations_distributed_cliques:
                distributed_cliques_skew_list = []
                for OA_clique in distributed_cliques:
                    distributed_cliques_skew_list.append(
                        utils.skew(OA_clique, D))
                combinations_distributed_cliques_avg_skew.append(
                    np.array(distributed_cliques_skew_list).mean()
                )
                combinations_distributed_cliques_skew.append(
                    distributed_cliques_skew_list)

            index_min_avg_skew = np.argmin(
                np.array(combinations_distributed_cliques_avg_skew))
            OA_avg_skew_cliques[i].append(
                combinations_distributed_cliques_avg_skew[index_min_avg_skew])

            print(f"OA - Min avg skew in DC:"
                  f" {combinations_distributed_cliques_avg_skew[index_min_avg_skew]}\n")

            OA_cliques_skew_list_individual_n_nodes_plot_info[i].append(
                combinations_distributed_cliques_skew[index_min_avg_skew])

    GA_cliques_skew_list_avg = []
    for n_nodes_results in \
            GA_cliques_skew_list_individual_n_nodes_plot_info:
        GA_cliques_skew_arrays = [np.array(x) for x in n_nodes_results]
        GA_cliques_skew_list_avg.append([np.mean(z) for z in
                                         zip(*GA_cliques_skew_arrays)])
    GA_average_skew_list_avg = []
    GA_max_skew_list_avg = []
    GA_min_skew_list_avg = []
    for index, _ in enumerate(N_NODES_LIST):
        GA_avg_skew_evolution_arrays = [np.array(x) for x in
                                        GA_avg_skew_evolution_individual_n_nodes_plot_info[
                                            index]]
        GA_average_skew_list_avg.append([np.mean(k) for k in
                                         zip(*GA_avg_skew_evolution_arrays)])
        GA_max_skew_evolution_arrays = [np.array(x) for x in
                                        GA_max_skew_evolution_individual_n_nodes_plot_info[
                                            index]]
        GA_max_skew_list_avg.append([np.mean(k) for k in
                                     zip(*GA_max_skew_evolution_arrays)])
        GA_min_skew_evolution_arrays = [np.array(x) for x in
                                        GA_min_skew_evolution_individual_n_nodes_plot_info[
                                            index]]
        GA_min_skew_list_avg.append([np.mean(k) for k in
                                     zip(*GA_min_skew_evolution_arrays)])
    for index, N_NODES in enumerate(N_NODES_LIST):
        GA_skew_distribution_conf = {
            "title": "",
            "x_label": "Skew",
            "y_label": "Number of cliques",
            "cliques_skew_list": GA_cliques_skew_list_avg[index],
            "pictures_folder_path": PICTURES_FOLDER_PATH,
            "n_nodes": N_NODES
        }
        GA_conf_skew_distribution_list.append(GA_skew_distribution_conf)
        '''
        GA_DC_individual_n_nodes_plot_info
        GA_cliques_distribution_conf = {
            "title": "Cliques distribution",
            "x_label": "Classes",
            "y_label": "Distribution",
            "distributed_cliques": GA_DC,
            "global_distribution": D,
            "n_nodes": N_NODES,
            "pictures_folder_path": PICTURES_FOLDER_PATH
        }
        '''
        # GA_avg_skew_evolution_arrays = [np.array(x, dtype=object) for x in
        #                                 GA_avg_skew_evolution_individual_n_nodes_plot_info]
        # GA_average_skew_list_avg = [np.mean(k) for k in zip(
        #     *GA_avg_skew_evolution_arrays)]
        # GA_max_skew_evolution_arrays = [np.array(x, dtype=object) for x in
        #                                 GA_max_skew_evolution_individual_n_nodes_plot_info]
        # GA_max_skew_list_avg = [np.mean(k) for k in zip(
        #     *GA_max_skew_evolution_arrays)]
        # GA_min_skew_evolution_arrays = [np.array(x, dtype=object) for x in
        #                                 GA_min_skew_evolution_individual_n_nodes_plot_info]
        # GA_min_skew_list_avg = [np.mean(k) for k in zip(
        #     *GA_min_skew_evolution_arrays)]
        GA_skew_evolution_conf = {
            "title": "",
            "x_label": "Number of nodes",
            "y_label": "Average skew",
            "average_skew": GA_average_skew_list_avg[index],
            "max_skew": GA_max_skew_list_avg[index],
            "min_skew": GA_min_skew_list_avg[index],
            "min_y_axis": 0.00,
            "max_y_axis": 0.95,
            "step_y_axis": 0.05,
            "n_nodes": N_NODES,
            "pictures_folder_path": PICTURES_FOLDER_PATH
        }
        '''
        GA_nodes_incidence_conf = {
            "title": "Incidence of number of nodes a clique over average skew",
            "x_label": "Number of nodes added",
            "y_label": "Average skew",
            "n_nodes_list": GA_n_nodes_list[GA_sorted_n_nodes_index],
            "average_skew_list":
                np.array(GA_skew_average_of_cliques_with_same_n_nodes_list)[
                    GA_sorted_n_nodes_index],
            "max_skew_list": np.array(GA_skew_max)[GA_sorted_n_nodes_index],
            "GA_min_skew_list": np.array(GA_skew_min)[GA_sorted_n_nodes_index],
            "pictures_folder_path": PICTURES_FOLDER_PATH,
            "n_nodes": N_NODES
        }
        '''

        print(f"N_nodes: {N_NODES}, K: {K}")
        print(
            f"GA - Skew avg: {np.array(GA_skew_average_of_cliques_with_same_n_nodes_list).mean()}")
        if PLOT:
            # utils.plot_cliques_distribution(GA_cliques_distribution_conf,
            #                                 alg_type='GA',
            #                                 save=SAVE_PLOTS)
            utils.plot_skew_evolution(GA_skew_evolution_conf,
                                      alg_type='GA',
                                      save=SAVE_PLOTS)
            # utils.plot_incidence_of_nodes_in_average_skew(
            #     GA_nodes_incidence_conf,
            #     # alg_type='GA',
            #     save=SAVE_PLOTS)
            utils.plot_skew_distribution_over_cliques(GA_skew_distribution_conf,
                                                      alg_type='GA',
                                                      save=SAVE_PLOTS)

        '''OPTIMAL'''
        distributed_cliques_skew_list_avg = []
        for n_nodes_results in \
                OA_cliques_skew_list_individual_n_nodes_plot_info:
            OA_cliques_skew_arrays = [np.array(x) for x in n_nodes_results]
            distributed_cliques_skew_list_avg.append([np.mean(k) for k in
                                             zip(*OA_cliques_skew_arrays)])
        # OA_cliques_skew_arrays = [np.array(x) for x in
        #                           OA_cliques_skew_list_individual_n_nodes_plot_info]
        # distributed_cliques_skew_list_avg = [np.mean(k) for k in zip(
        #     *OA_cliques_skew_arrays)]
        OA_skew_distribution_conf = {
            "title": f"{N_NODES} NODES X {N_CLASSES} CLASSES",
            "x_label": "Skew",
            "y_label": "Number of cliques",
            "cliques_skew_list": distributed_cliques_skew_list_avg[index],
            "pictures_folder_path": PICTURES_FOLDER_PATH,
            "n_nodes": N_NODES
        }
        OA_conf_skew_distribution_list.append(OA_skew_distribution_conf)

        individual_skew_dist_over_cliques_conf = {
            "title": OA_skew_distribution_conf["title"],
            "x_label": OA_skew_distribution_conf["x_label"],
            "y_label": OA_skew_distribution_conf["y_label"],
            "GA_skew_distribution": GA_skew_distribution_conf[
                "cliques_skew_list"],
            "OA_skew_distribution": OA_skew_distribution_conf[
                "cliques_skew_list"],
            "n_nodes": N_NODES,
            "pictures_folder_path": PICTURES_FOLDER_PATH
        }

        if PLOT:
            utils.plot_skew_distribution_over_cliques(OA_skew_distribution_conf,
                                                      alg_type="OA",
                                                      save=SAVE_PLOTS)
            utils.plot_individual_skew_distribution_over_cliques(
                individual_skew_dist_over_cliques_conf,
                save=SAVE_PLOTS)

    GA_time_avg = [np.array(list).mean() for list in GA_time]
    OA_time_avg = [np.array(list).mean() for list in OA_time]
    GA_avg_skew_cliques_avg = [np.array(list).mean() for list in
                               GA_avg_skew_cliques]
    OA_avg_skew_cliques_avg = [np.array(list).mean() for list in
                               OA_avg_skew_cliques]
    OA_number_combinations_avg = [np.array(list).mean() for list in
                                  OA_number_combinations]
    skew_dist_over_cliques_conf = {
        "GA_conf_skew_distribution_list": GA_conf_skew_distribution_list,
        "OA_conf_skew_distribution_list": OA_conf_skew_distribution_list,
        "subplots_n_r": 3,
        "subplots_n_c": 4,
        "n_nodes": N_NODES_LIST
    }
    utils.plot_skew_distribution_over_cliques(skew_dist_over_cliques_conf,
                                              save=SAVE_PLOTS)

    avg_skew_distributed_cliques_conf = {
        "title": "",
        "x_label": "Number of nodes",
        "y_label": "Average skew of distributed cliques",
        "n_nodes_list": N_NODES_LIST,
        "GA_avg_skew_cliques": GA_avg_skew_cliques_avg,
        "OA_avg_skew_cliques": OA_avg_skew_cliques_avg,
        "pictures_folder_path": PICTURES_FOLDER_PATH
    }
    utils.plot_avg_skew_distributed_cliques(avg_skew_distributed_cliques_conf,
                                            save=SAVE_PLOTS)

    time_for_constructing_cliques_conf = {
        "title": "",
        "x_label": "Number of nodes",
        "y_label": "Time in seconds",
        "n_nodes_list": N_NODES_LIST,
        "GA_time": GA_time_avg,
        "OA_time": OA_time_avg,
        "pictures_folder_path": PICTURES_FOLDER_PATH
    }
    utils.plot_time_for_constructing_cliques(time_for_constructing_cliques_conf,
                                             save=SAVE_PLOTS)
    print(f"GA_time: {GA_time}")
    print(f"OA_time: {OA_time}")

    OA_number_combinations_conf = {
        "title": "Optimal algorithm",
        "x_label": "Number of nodes",
        "y_label": "Number of combinations",
        "n_nodes_list": N_NODES_LIST,
        "number_combinations": OA_number_combinations_avg,
        "pictures_folder_path": PICTURES_FOLDER_PATH
    }
    utils.plot_number_combinations_opt_alg(OA_number_combinations_conf,
                                           save=SAVE_PLOTS)
