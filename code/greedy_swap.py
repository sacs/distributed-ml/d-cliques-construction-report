import numpy as np
from random import Random

from pathlib import Path
import matplotlib.pyplot as plt

import constants
import functions


def greedy_swap(n, max_clique_size, max_steps=None, seed=10):
    """
    This function was copied from:
    https://gitlab.epfl.ch/sacs/distributed-ml/non-iid-topology-simulator/-/blob/master/tools/setup/topology/d_cliques/greedy_swap.py
    :param n: list of nodes
    :param max_clique_size: constraint
    :param max_steps:
    :param seed:
    :return: list of lists of skews of distributed cliques over iterations
    """
    # Random assignment
    ranks = set([rank for rank in range(len(n))])
    rand = Random()
    rand.seed(seed)

    cliques = []
    while len(ranks) > max_clique_size:
        c = rand.sample(ranks, max_clique_size)
        ranks.difference_update(c)
        cliques.append(set(c))
    cliques.append(set(ranks))

    global_dist = n.sum(axis=0) / n.sum(axis=0).sum()

    skews_over_iterations = []
    rand = Random()
    rand.seed(seed)
    if max_steps is None:
        max_steps = constants.ExperimentSetting.TESTS
    for k in range(max_steps):
        c1, c2 = rand.sample(cliques, 2)
        c1_distribution = np.array([n[r] for r in c1]).sum(axis=0) / \
                          np.array([n[r] for r in c1]).sum(axis=0).sum()
        c1_skew = np.abs(c1_distribution - global_dist).sum()
        c2_distribution = np.array([n[r] for r in c2]).sum(axis=0) / \
                          np.array([n[r] for r in c2]).sum(axis=0).sum()
        c2_skew = np.abs(c2_distribution - global_dist).sum()
        baseline = c1_skew + c2_skew

        def updated(n1, c1, n2, c2):
            c1_updated = c1.difference([n1]).union([n2])
            c2_updated = c2.difference([n2]).union([n1])
            c1_updated_d = np.array([n[r] for r in c1_updated]).sum(axis=0) / \
                           np.array([n[r] for r in c1_updated]).sum(
                               axis=0).sum()
            c1_updated_skew = np.abs(c1_updated_d - global_dist).sum()
            c2_updated_d = np.array([n[r] for r in c2_updated]).sum(axis=0) / \
                           np.array([n[r] for r in c2_updated]).sum(
                               axis=0).sum()
            c2_updated_skew = np.abs(c2_updated_d - global_dist).sum()
            return c1_updated_skew + c2_updated_skew

        pairs = [(n1, n2, updated(n1, c1, n2, c2) - baseline) for n1 in c1 for
                 n2 in c2]
        improving = [p for p in pairs if p[2] < 0]
        if len(improving) > 0:
            p = rand.sample(improving, 1)[0]
            c1.remove(p[0])
            c1.add(p[1])
            c2.remove(p[1])
            c2.add(p[0])

        # stats
        skews = [
            np.abs((np.array([n[r] for r in c]).sum(axis=0) /
                    np.array([n[r] for r in c]).sum(axis=0).sum()) -
                   global_dist).sum()
            for c in cliques]
        skews_over_iterations.append(np.array(skews))
        last = k
        print(f"k: {last} -> skews: {skews}")

    return np.array(skews_over_iterations)


if __name__ == "__main__":
    rng = np.random.default_rng(20)
    MAX_NODES_PER_CLIQUE = 20
    MAX_STEPS = 200
    PICTURES_FOLDER_PATH = "../pictures/greedy_swap/"
    PLOT_NAME = f"greedy_swap"
    N = functions.data_partition(constants.ExperimentSetting.N_NODES,
                                 constants.ExperimentSetting.N_CLASSES, rng,
                                 constants.ExperimentSetting.SHARD_SIZE,
                                 constants.ExperimentSetting.SAMPLES_PER_CLASS)

    constraints_info = {}
    for i in range(len(constants.Constraints.MAX_NODES_PER_CLIQUE)):
        skews_over_iterations = greedy_swap(
            N, constants.Constraints.MAX_NODES_PER_CLIQUE[i], MAX_STEPS,
            seed=10)
        constraints_info[constants.Constraints.MAX_NODES_PER_CLIQUE[i]] = [
            skews_over_iterations.mean(axis=1),
            skews_over_iterations.min(axis=1),
            skews_over_iterations.max(axis=1),
            skews_over_iterations[-1]
        ]

    '''
    Plotting greedy swap converge speed
    '''
    # constraints_to_plot = [5, 10, 20, 30, 50, 70]
    # rows, cols = 2, 3
    # counter = 0
    # fig, axs = plt.subplots(rows, cols, sharey=True, sharex=True)
    # for i in range(rows):
    #     for j in range(cols):
    #         avg_skews, min_skews, max_skews, _ = constraints_info[
    #             constraints_to_plot[counter]]
    #         axs[i, j].plot(range(1, MAX_STEPS + 1),
    #                        avg_skews, color=constants.Figures.COLOR_PRIMARY)
    #         axs[i, j].plot(range(1, MAX_STEPS + 1),
    #                        max_skews, color=constants.Figures.COLOR_PRIMARY,
    #                        alpha=0.4)
    #         axs[i, j].plot(range(1, MAX_STEPS + 1),
    #                        min_skews, color=constants.Figures.COLOR_PRIMARY,
    #                        alpha=0.4)
    #         axs[i, j].set_title(f"Max clique size:"
    #                             f" {constraints_to_plot[counter]}")
    #         axs[i, j].set_xlabel("Number of iterations")
    #         axs[i, j].set_ylabel("Average skew")
    #         axs[i, j].grid(True)
    #         counter += 1
    # fig.tight_layout()
    # Path(f"{PICTURES_FOLDER_PATH}convergence_speed/").mkdir(parents=True,
    #                                                         exist_ok=True)
    # plt.savefig(
    #     f"{PICTURES_FOLDER_PATH}convergence_speed/convergence_speed"
    #     f"_{PLOT_NAME}_"
    #     f"{constants.ExperimentSetting.DATA_PARTITION_MODE}_partition_"
    #     f"{constants.ExperimentSetting.N_NODES}_nodes.pdf")
    # plt.show()

    '''
    Plotting average skew dependency from constraints 
    '''
    avg_skews, min_skews, max_skews = [], [], []
    for skews_info in constraints_info.values():
        print(skews_info)
        avg_skews.append(skews_info[-1].mean())
        min_skews.append(skews_info[-1].min())
        max_skews.append(skews_info[-1].max())
    plt.figure().clear()
    plt.xlabel("Max number of nodes in a clique")
    plt.ylabel("Average skew")
    plt.plot(constants.Constraints.MAX_NODES_PER_CLIQUE, avg_skews,
             color=constants.Figures.COLOR_PRIMARY)
    plt.plot(constants.Constraints.MAX_NODES_PER_CLIQUE, min_skews,
             color=constants.Figures.COLOR_PRIMARY, alpha=0.4)
    plt.plot(constants.Constraints.MAX_NODES_PER_CLIQUE, max_skews,
             color=constants.Figures.COLOR_PRIMARY, alpha=0.4)
    Path(f"{PICTURES_FOLDER_PATH}average_skew_iterations/").mkdir(
        parents=True, exist_ok=True)
    plt.grid(True)
    plt.savefig(
        f"{PICTURES_FOLDER_PATH}average_skew_iterations/skew_size_of_cliques_"
        f"{PLOT_NAME}_{constants.ExperimentSetting.DATA_PARTITION_MODE}_"
        f"partition_{constants.ExperimentSetting.N_NODES}_nodes.pdf",
        bbox_inches='tight')
    plt.show()
