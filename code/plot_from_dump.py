import pickle
import functions

with open("centr_times_100_points_6_tests", "rb") as file:
    times = pickle.load(file)

LEFT = 10
RIGHT = int(1e6)
nodes_range = range(LEFT, RIGHT + 1, 10000)

functions.plot_times(times, nodes_range)
