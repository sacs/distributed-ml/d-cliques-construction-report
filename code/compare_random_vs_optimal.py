import numpy as np
import matplotlib.pyplot as plt

import decentr_distr_random_cliques as drc
import optimal_greedy_pre_comp_D as optGreedy
import utils as utils

if __name__ == "__main__":
    # COLORMAP = {(600, 10): "blue", (600, 50): "orange", (6000, 10): "red",
    #             (6000, 50): "green"}
    N_CLASSES = 10
    N_NODES = 10
    COLORMAP = {(N_NODES, N_CLASSES): "blue"}
    RANDOM_GENERATOR = np.random.default_rng(69)
    PICTURES_FOLDER_PATH = "../test_pictures/"
    PLOT = True
    SAVE_PLOTS = False
    K = range(1, N_NODES + 1)  # range(2, N_NODES+1)
    MAX_NODES_PER_CLIQUE = range(1, 12)

    N = utils.create_nodes(RANDOM_GENERATOR, N_CLASSES, N_NODES)
    D = utils.get_global_distribution(N, N_CLASSES)

    distributed_cliques_skew_list = optGreedy.optimal_greedy(N, D,
                                                             MAX_NODES_PER_CLIQUE, )

    fig, axs = plt.subplots(figsize=(6, 6))
    axs.set_xlabel("Number of iterations")
    axs.grid(True)
    for N in [N_NODES]:
        for CLASSES in [N_CLASSES]:
            configurations, average_skew, max_skew, min_skew, clique_distribution \
                = drc.run_random_assignment(N, CLASSES, RANDOM_GENERATOR,
                                            configurations=range(1, 13))

            axs.plot(configurations, average_skew, c=COLORMAP[(N, CLASSES)],
                     label="N = " + str(N) + ", " + str(CLASSES) + " classes")
            print(N, CLASSES, min_skew[-1])
            print(average_skew[-1])
            print(max_skew[-1])
            axs.set_ylim(0, 0.5)
            axs.set_ylabel("Average skew")
            axs.set_xlabel("Number of nodes in a clique")
            axs.plot(configurations, min_skew, alpha=0.4,
                     c=COLORMAP[(N, CLASSES)])
            axs.plot(configurations, max_skew, alpha=0.4,
                     c=COLORMAP[(N, CLASSES)])
    plt.plot(MAX_NODES_PER_CLIQUE, distributed_cliques_skew_list)
    name = "Skew_size_of_clique_optimal_vs_random"
    axs.legend(loc='best')
    # plt.savefig("../test_pictures/" + name + ".pdf", bbox_inches='tight')
    plt.show()
