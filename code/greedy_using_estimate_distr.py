import numpy as np
import matplotlib.pyplot as plt

import greedy_with_pre_comp_of_D as greedy_funcs
import functions


def dfs_with_creating_cliques(s, graph, visited, distributed_cliques,
                              average_skew_history):
    if visited[s]:
        return
    visited[s] = True
    # print(s)
    greedy_funcs.one_iteration_of_alg4(distributed_cliques,
                                       all_nodes[s].average_global_samples,
                                       all_nodes[s].samples,
                                       average_skew_iterations=average_skew_history)
    for i in range(len(graph[s])):
        dfs_with_creating_cliques(graph[s][i], graph, visited,
                                  distributed_cliques, average_skew_history)


if __name__ == "__main__":
    N_CLASSES = 10
    N_NODES = 100
    TESTS = 10
    rng = np.random.default_rng(24)
    results_tests = np.zeros((2, TESTS, N_NODES))

    for i in range(TESTS):
        RANDOM_GENERATOR = np.random.default_rng(rng.integers(100000))
        result = functions.build_graph_run_pushsum({"n": 10,
                                                    "topology": "GRID",
                                                    "m": 10,
                                                    "rng": RANDOM_GENERATOR},
                                                   N_CLASSES, RANDOM_GENERATOR,
                                                   100)
        all_nodes, average_skew, graph, max_skew, min_skew = result
        N = np.zeros((N_NODES, N_CLASSES))
        for j in range(len(all_nodes)):
            N[j] += all_nodes[j].samples
        D = greedy_funcs.get_global_distribution(N, N_CLASSES)
        visited = [False] * len(all_nodes)
        DC_estimate = []
        average_skew_history = []
        dfs_with_creating_cliques(0, graph, visited, DC_estimate,
                                  average_skew_history)
        result = greedy_funcs.create_cliques_and_get_skew_statistics(N, D)
        DC, skew_iterations_results = result
        results_tests[0, i] = skew_iterations_results[0]
        results_tests[1, i] = average_skew_history
        average_skew_dd = greedy_funcs.get_statistics_skew_from_distributed_cliques(
            DC, D)
        assert np.isclose(average_skew_dd[0], results_tests[0, i, -1])
        average_skew_ed = greedy_funcs.get_statistics_skew_from_distributed_cliques(
            DC_estimate, D)
        print("DD:", average_skew_dd)
        print("ED:", average_skew_ed)
    plt.plot(results_tests[0].mean(axis=0), label="true", c='blue')
    plt.plot(results_tests[1].mean(axis=0), label="estimated", c='orange')
    plt.plot(results_tests[0].min(axis=0), c='blue', alpha=0.4)
    plt.plot(results_tests[0].max(axis=0), c='blue', alpha=0.4)
    plt.plot(results_tests[1].max(axis=0), c='orange', alpha=0.4)
    plt.plot(results_tests[1].min(axis=0), c='orange', alpha=0.4)
    plt.legend(loc='best')
    plt.xlabel("Number of iteratons")
    plt.ylabel("Average skew")
    plt.ylim(0, 0.5)
    plt.grid(True)
    plt.show()
