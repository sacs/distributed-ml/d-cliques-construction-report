import numpy as np
import matplotlib.pyplot as plt
import functions


def get_number_of_messages_and_skew(send_all_neighbours, conf, iterations):
    all_nodes = []
    distribution = np.zeros(CLASSES)
    for i in range(len(graph)):
        all_nodes.append(functions.Node(rng, graph[i], CLASSES,
                                        starting_node=True,
                                        send_all_neighbours=send_all_neighbours
                                        ))
        distribution += all_nodes[-1].samples
    distribution /= distribution.sum()
    number_of_messages = []
    average_skew = []
    for iters in range(iterations[conf]):
        curr_skew = 0
        n = 0
        for i in range(len(graph)):
            n += all_nodes[i].communication_round(all_nodes)
            curr_skew += np.abs(all_nodes[i].average_global_samples / all_nodes[
                i].average_global_samples.sum() - distribution).sum()
        curr_skew /= len(graph)
        average_skew.append(curr_skew)
        if iters > 0:
            number_of_messages.append(n + number_of_messages[-1])
        else:
            number_of_messages.append(n)
        for i in range(len(graph)):
            all_nodes[i].finish_round()
    return number_of_messages, average_skew


rng = np.random.default_rng(1)

CLASSES = 10

# configurations = [{"n": 400, "topology": "RING"},
#                   {"n": 20, "topology": "GRID", "m": 20},
#                   {"n": 400, "topology": "FC"},
#                   {"n": 400, "topology": "RANDOM", "m": 10, "rng": rng}]

configurations = [{"n": 100, "topology": "RING"},
                  {"n": 10, "topology": "GRID", "m": 10},
                  {"n": 100, "topology": "FC"},
                  {"n": 100, "topology": "RANDOM", "m": 10, "rng": rng}]

iterations = [100, 100, 20, 20]
iterations_fc = [50, 25, 3, 10]

fig, axs = plt.subplots(2, 2, figsize=(12, 12))

# skew_to_uniform = functions.mean_skew_N(configurations[0]["n"], CLASSES, 10,
#                                         rng)

for conf in range(len(configurations)):
    graph = functions.build_graph(**configurations[conf])
    messages_gossip, skew_gossip = get_number_of_messages_and_skew(False, conf,
                                                                   iterations)
    messages_all, skew_all = get_number_of_messages_and_skew(True, conf,
                                                             iterations_fc)

    axs[conf // 2, conf % 2].grid(True)
    title = functions.get_title(configurations, conf)
    axs[conf // 2, conf % 2].set_title(title)
    axs[conf // 2, conf % 2].set_xlabel("Number of messages")
    axs[conf // 2, conf % 2].plot([0] + messages_gossip[:-1], skew_gossip,
                                  label="to 1 neigh, " + str(iterations[conf]))
    axs[conf // 2, conf % 2].scatter([0] + messages_gossip[:-1], skew_gossip)
    axs[conf // 2, conf % 2].plot([0] + messages_all[:-1], skew_all,
                                  label="to all neigh, " + str(
                                      iterations_fc[conf]))
    axs[conf // 2, conf % 2].scatter([0] + messages_all[:-1], skew_all)
    axs[conf // 2, conf % 2].set_ylim(0, 0.5)
    axs[conf // 2, conf % 2].set_ylabel("Average skew")
    axs[conf // 2, conf % 2].legend(loc='best')
    # axs[conf // 2, conf % 2].axhline(skew_to_uniform, linestyle='--',
    #                                  c='black')
name = "TWO_SKEWS_N_" + str(configurations[0]["n"]) + "_CLASSES_" + \
       str(CLASSES)
plt.savefig("../test_pictures/" + name + ".pdf", bbox_inches='tight')
plt.show()
