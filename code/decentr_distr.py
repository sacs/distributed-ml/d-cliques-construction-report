import numpy as np
import matplotlib.pyplot as plt
import functions

COLORMAP = ['blue', 'orange', 'red', 'green', 'black', 'yellow', 'purple']
rng = np.random.default_rng(3)

CLASSES = 10
# WHAT_TO_PLOT = "SKEW"
# WHAT_TO_PLOT = "TIMES"
WHAT_TO_PLOT = "MIN_MAX_SKEW"
# WHAT_TO_PLOT = "DIRECTED_DIFFERENCE"
# WHAT_TO_PLOT = "PushSumWeights"
# 1
# configurations = [{"n": 400, "topology": "RING"},
#                   {"n": 20, "topology": "GRID", "m": 20},
#                   {"n": 400, "topology": "FC"},
#                   {"n": 400, "topology": "RANDOM", "m": 10, "rng": rng}]
# 2
# configurations = [{"n": 100, "topology": "FC"}]
# 3
# configurations = [{"n": 100, "topology": "RING"},
#                   {"n": 10, "topology": "GRID", "m": 10},
#                   {"n": 100, "topology": "FC"},
#                   {"n": 100, "topology": "RANDOM", "m": 10, "rng": rng}]
# 4
# configurations = [{"n": 1000, "topology": "RING"},
#                   {"n": 31, "topology": "GRID", "m": 31},
#                   {"n": 1000, "topology": "FC"},
#                   {"n": 1000, "topology": "RANDOM", "m": 10, "rng": rng}]
# 5
# configurations = [{"n": 3, "topology": "FC"},
#                   {"n": 10, "topology": "FC"},
#                   {"n": 100, "topology": "FC"},
#                   {"n": 1000, "topology": "FC"}]
# 6
# configurations = [{"n": 10, "topology": "RING"}]
# 7
# configurations = []
# MAX_NEIGH = 10
# TESTS = 5
# for i in range(1, MAX_NEIGH + 1):
#     for _ in range(TESTS):
#         configurations.append({"n": 100, "topology": "RANDOM", "m": i,
#                                "rng": rng})
#         configurations.append({"n": 100, "topology": "RANDOM_DIRECTED", "m": i,
#                                "rng": rng})
# 8
# configurations = [{"n": 100, "topology": "RANDOM", "m": 2, "rng": rng},
#                   {"n": 100, "topology": "RANDOM", "m": 3, "rng": rng},
#                   {"n": 100, "topology": "RANDOM", "m": 99, "rng": rng},
#                   {"n": 100, "topology": "RANDOM", "m": 10, "rng": rng}]
# 9
# configurations = [{"n": 100, "topology": "RANDOM", "m": 2, "rng": rng,
#                    "connected": False},
#                   {"n": 100, "topology": "RANDOM", "m": 3, "rng": rng,
#                    "connected": False}]
# 10
# configurations = [{"n": 100, "topology": "RANDOM", "m": 2, "rng": rng},
#                   {"n": 100, "topology": "RANDOM_DIRECTED", "m": 2,
#                    "rng": rng}]
# 11
configurations = [{"n": 100, "topology": "RANDOM", "m": 2, "rng": rng,
                   "connected": True},
                  {"n": 100, "topology": "RANDOM", "m": 3, "rng": rng,
                   "connected": True}]
# 12
# configurations = [{"n": 100, "topology": "RANDOM", "m": 2, "rng": rng,
#                    "connected": True},
#                   {"n": 100, "topology": "RANDOM", "m": 3, "rng": rng,
#                    "connected": True},
#                   {"n": 100, "topology": "RANDOM", "m": 99, "rng": rng,
#                    "connected": True},
#                   {"n": 100, "topology": "RANDOM", "m": 10, "rng": rng,
#                    "connected": True}]

iterations = [100] * len(configurations)

if WHAT_TO_PLOT == "SKEW":
    fig, axs = plt.subplots(figsize=(6, 6))
    axs.set_xlabel("Number of iterations")
    axs.grid(True)
elif WHAT_TO_PLOT == "TIMES":
    fig, axs = plt.subplots(figsize=(7, 7))
elif WHAT_TO_PLOT == "MIN_MAX_SKEW":
    if len(configurations) == 4:
        fig, axs = plt.subplots(2, 2, figsize=(8, 8))
    elif len(configurations) == 2:
        fig, axs = plt.subplots(1, 2, figsize=(8, 4))
    else:
        raise ValueError("Wrong number of configurations")
elif WHAT_TO_PLOT == "DIRECTED_DIFFERENCE":
    fig, axs = plt.subplots(figsize=(7, 7))
    axs.set_xlabel("Number of neighbours")
    axs.grid(True)
    directed_results, undirected_results = [], []
    prev = -1
elif WHAT_TO_PLOT == "PushSumWeights":
    fig, axs = plt.subplots(2, 2, figsize=(10, 10))
else:
    raise ValueError("Unrecognized WHAT_TO_PLOT")


# skew_to_uniform = functions.mean_skew_N(configurations[0]["n"], CLASSES, 10,
#                                         rng)


def plot_min_max_skew(axs, title):
    axs.grid(True)
    axs.plot(average_skew, c=COLORMAP[conf])
    axs.set_ylim(0, 0.5)
    axs.set_ylabel("Average skew")
    axs.set_title(title)
    axs.plot(min_skew, c=COLORMAP[conf], alpha=0.4)
    axs.plot(max_skew, c=COLORMAP[conf], alpha=0.4)


for conf in range(len(configurations)):
    print(conf)
    rng_for_data = np.random.default_rng(1)
    all_nodes, average_skew, graph, max_skew, min_skew = functions.build_graph_run_pushsum(
        configurations[conf], CLASSES, rng_for_data, iterations[conf])
    # print(functions.get_number_of_components(graph))
    title = functions.get_title(configurations, conf)
    if WHAT_TO_PLOT == "PushSumWeights":
        weights = []
        for i in range(len(all_nodes)):
            s, w = 0, 0
            for j in range(len(all_nodes[i].prev_info)):
                s += all_nodes[i].prev_info[j][0]
                w += all_nodes[i].prev_info[j][1]
            weights.append(w)
        axs[conf // 2, conf % 2].hist(weights)
        axs[conf // 2, conf % 2].set_title(title)
        axs[conf // 2, conf % 2].set_xlabel("Weights")
        axs[conf // 2, conf % 2].set_ylabel("Number of nodes")
    elif WHAT_TO_PLOT == "TIMES":
        times_by_iters = np.zeros((len(graph), iterations[conf]))
        for i in range(len(all_nodes)):
            times_by_iters[i] = all_nodes[i].time_by_iteration
        axs.set_xlabel("Number of iterations")
        axs.grid(True)
        overall_mean = times_by_iters.mean()
        axs.plot(times_by_iters.mean(axis=0),
                 label="Overall mean: " + f'{overall_mean:9.5f}')
        axs.fill_between(range(iterations[conf]),
                         times_by_iters.min(axis=0),
                         times_by_iters.max(axis=0),
                         color='#888888', alpha=0.4)
        axs.legend(loc='best')
        axs.set_ylabel("Time (sec)")
    elif WHAT_TO_PLOT == "SKEW":
        axs.plot(average_skew, label=title, c=COLORMAP[conf])
    elif WHAT_TO_PLOT == "MIN_MAX_SKEW":
        if len(configurations) == 4:
            plot_min_max_skew(axs[conf // 2, conf % 2], title)
        elif len(configurations) == 2:
            plot_min_max_skew(axs[conf % 2], title)
        # axs[conf // 2, conf % 2].axhline(skew_to_uniform, linestyle='--',
        #                                  c='black')
    elif WHAT_TO_PLOT == "DIRECTED_DIFFERENCE":
        if conf % 2 == 0:
            if conf % (2 * TESTS) == 0:
                undirected_results.append(average_skew[-1])
            else:
                undirected_results[-1] += average_skew[-1]
        else:
            if conf % (2 * TESTS) == 1:
                directed_results.append(average_skew[-1])
            else:
                directed_results[-1] += average_skew[-1]
            if (conf + 1) % (2 * TESTS) == 0:
                directed_results[-1] /= TESTS
                undirected_results[-1] /= TESTS
    else:
        raise ValueError("Wrong " + WHAT_TO_PLOT)
if WHAT_TO_PLOT == "SKEW":
    axs.legend(loc='best')
    axs.set_ylim(0, 0.5)
    axs.set_ylabel("Average skew")
    # axs.axhline(skew_to_uniform, linestyle='--', c='black')
elif WHAT_TO_PLOT == "DIRECTED_DIFFERENCE":
    axs.plot(range(1, MAX_NEIGH + 1), directed_results, label="directed")
    axs.plot(range(1, MAX_NEIGH + 1), undirected_results, label="undirected")
    axs.legend(loc='best')
    axs.set_ylabel("Average skew after 100 iterations")
name = WHAT_TO_PLOT + "_N_" + str(configurations[0]["n"]) + "_CLASSES_" \
       + str(CLASSES)
if configurations[0]["topology"] == "RANDOM":
    name += "_RANDOM_" + str(len(configurations)) + "_" + configurations[1][
        "topology"]
if "connected" in configurations[0]:
    name += "_" + str(configurations[0]["connected"])
plt.savefig("../pictures/" + name + ".pdf", bbox_inches='tight')
plt.show()
