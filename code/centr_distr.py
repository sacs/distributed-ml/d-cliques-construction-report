import numpy as np
import time
import pickle
import functions

rng = np.random.default_rng(0)

CLASSES = 10
LEFT = 10
RIGHT = int(1e6)
nodes_range = range(LEFT, RIGHT + 1, 10000)
TESTS = 6

distribution = np.zeros(CLASSES)
times = np.zeros((len(nodes_range), TESTS))
for j in range(len(nodes_range)):
    for k in range(TESTS):
        N = nodes_range[j]
        N_distr = rng.uniform(100, 10001, (N, CLASSES))
        start_time = time.time()
        for i in range(N):
            distribution += N_distr[i]
        distribution /= distribution.sum()
        times[j][k] = time.time() - start_time
with open("centr_times_" + str(len(nodes_range)) + "_points_6_tests", "wb") \
        as file:
    pickle.dump(times, file)

functions.plot_times(times, nodes_range)
